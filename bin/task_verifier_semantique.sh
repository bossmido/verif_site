#!/bin/bash


task_verifier_semantique_1(){
    table_table_table=1;
    table_table_table=$( $PUP -f $TMP/tmp.html 'table table table table table');
    if [[  ${#table_table_table} -gt 0 ]]; then
        echo -e "utilisation_de_balises_table_pour_le_site";
    else
        echo -e "pas_de_probleme_de_structuration_de_code";
    fi
}

fn_exists()
{
    test_var=$( type -t  $1   | wc -c );
    export test_exists=0;
    if [[  $test_var -gt 0 ]] ; then
        test_exists=0;
    else
        test_exists=1;
    fi
    # test_var=$(echo  -e $test_var |  grep -q '^function$' 2>/dev/null );
    # if [[  $test_exists -eq 0 ]] ; then
    #     if [[  $test_var -eq 0 ]] ; then
    #         test_exists=0;
    #     else
    #         test_exists=1;
    #     fi
    # else
    #     test_exists=1;
    # fi
    echo -e $test_exists;
}
task_verifier_semantique(){
    local inc=1;
    local task_nom="task_verifier_semantique_";
    local retour="";

    while (( "$(fn_exists "${task_nom}${inc}")"  == "0" )); do
        commande_serie=${task_nom}${inc};
        retour=$($commande_serie $@);
        log2 "debut";
        log2 $retour;
        log2 "fin";
        inc=$(($inc+1));
    done
    echo -e $retour;
}
