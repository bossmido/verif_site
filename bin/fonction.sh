#!/bin/bash

### Pretty-print dedicated (array) var, MAPFILE.
prettyPrintMAPFILE() {
    let i=0
    echo "[MAPFILE]"
    for l in "${MAPFILE[@]}"
    do
        echo "$i.   |$l|"
        let i++
    done
    echo "[/MAPFILE]"
}

### Read the lines of the specified file into an array.
##      Skips blank lines. Trims leading & trailing spaces in every line.
##      Resulting array is in the dedicated var, MAPFILE.
##  Params:
##      1.  File location to be read (required).
##      2.  Comment delimiter (optional; default: '#').
##+         -- to delimit any text that is to be omitted. 
fileLines2Array() {
    if [[ -z "$1" ]] || [[ ! -e $1 ]]; then
        echo "le fichier n'existe pas."
        return 1
    else
        echo "Fichier à lire: $1"
    fi

    commentPattern="\#*"
    [[ -n "$2" ]] && commentPattern="\\$2*"
    [[ -n "$commentPattern" ]] && echo "  ... will skip lines and trailing portions of lines beginning with this pattern: '$commentPattern'."

    mapfile -t < $1
    
    let i=0
    for l in "${MAPFILE[@]}"
    do
        l=${l%%$commentPattern}     
        l=${l%%*( )}                
        l=${l##*( )}                
        if [[ -z "$l" ]]; then      
            unset MAPFILE[$i]
        else
            MAPFILE[$i]=$l          
        fi
        let i++
    done
    
    prettyPrintMAPFILE

    return 0
}


log() {
    if [[ $DEBUG == 1 ]]; then
       printf '%s\n' "$@" >> $BASE_MES_LOG/my.log
    fi
}
log2() {
   
       printf ' @@@@@  : %s \n' "$@">> $BASE_MES_LOG/my.log  
}


timestamp() {
    date +"%T";
}


debut_time(){
    interval=timestamp;
}
fin_timer(){
    interval=$timestamp - $interval;
    echo -e   interval;
}

afficher_label(){
    echo -e "\033[31mtest du site :";
    echo -e $1;
    echo -e "\n\n";
    echo -e "$2";
    echo -e "\n\e[0m";
}



verifier_val(){
    if [ -z $1  ]; then
       $1="";
    fi   
}

function sedeasy {
  sed -i "s/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/$(echo $2 | sed -e 's/[\/&]/\\&/g')/g" $3
}



escape_sed() {
 sed \
  -e 's/\//\\\//g' \
  -e 's/\&/\\\&/g'
}

upvar() {
    if unset -v "$1"; then        
        if (( $# == 2 )); then
            eval $1=\"\$2\"        
        else
            eval $1=\(\"\${@:2}\"\)
         fi
    fi
}

function charger_script() {

    source "./$BASE_BIN/$1";
}

test_declare2 () {
    declare -F f > /dev/null
}

test_type () {
    type -t f | grep -q 'function'
}

test_type2 () {
    local var=$(type -t f)
    [[ "${var-}" = function ]]
}