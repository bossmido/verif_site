#!/usr/bin/env python2.7
import sqlite3
import sys
import pprint

pp = pprint.PrettyPrinter(indent=4)

# -*- coding: utf-8 -*-


param_site=sys.argv[1:]
conn = sqlite3.connect('sortie/BDD.db')
c = conn.cursor()
try:

    requete = "SELECT * FROM site WHERE site_libelle LIKE '%"+str(param_site[0])+"%' ;"
    #requete = "SELECT * FROM site;"
    res=c.execute(requete)
    rows = c.fetchall()
    if rows != None and len(rows)>0 :
        sortie=list(rows[0])
    else:
        sortie=""
    print ';'.join(sortie)
except sqlite3.Error as er:
    print(param_site[0]+" : plusieurs fois dans la BDD")


finally:
    if conn:
        conn.close()