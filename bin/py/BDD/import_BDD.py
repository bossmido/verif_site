#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import sqlite3
import sys, getopt
import pprint
import csv
import os

from openpyxl import *
from openpyxl import worksheet
from openpyxl.drawing.fill import PatternFillProperties, ColorChoice
from openpyxl import Workbook
from openpyxl import load_workbook


pp = pprint.PrettyPrinter(indent=4)


def checkTableExists(dbcon, tablename):
    try:
        dbcur = dbcon.cursor()
        dbcur.execute("""
            SELECT COUNT(*)
            FROM information_schema.tables
            WHERE table_name = '{0}'
            """.format(tablename.replace('\'', '\'\'')))
        if dbcur.fetchone()[0] == 1:
            dbcur.close()
            return True

        dbcur.close()
        return False

    except sqlite3.OperationalError:
        dbcur.close()
        return True



conn = sqlite3.connect('../bin/BDD.db')
c = conn.cursor()

# param
#param = getopt.getopt(args)


param_site=sys.argv[1:]

try:

    c.execute("SELECT * FROM site")

    rows = c.fetchall()    

    pp.pprint(rows)
    if os.path.exists('../sortie/resultat.csv'):
        os.remove('../sortie/resultat.csv')
    fichier_csv = open('../sortie/resultat.csv',"a")
    writer = csv.writer(fichier_csv,delimiter =';')
    #writer.writerow(("url","404notfound","robot.txt","sitemap","description#title","semantique","commentaire","site_libelle"))
    #
    wb = load_workbook(filename='../sortie/resultat.xlsx')
    wb.get_sheet_names()
    std=wb['resultat_csv']

    liste_xls=[]
    inc=1
    inc_lettre='A'

    while((std[inc_lettre+str(inc)].value != None )or(inc == 3)):
        liste_xls.append([]);
        for j  in range(ord("A"),ord("H")):
            inc_lettre=str(unichr(j))
            if(inc_lettre=="G"):
                commentaire =wb['resultat_affichage']
                pos="C"+str((inc*8)+1)
                #print(pos)
                print("\n\n\n\n")
                print("debut  du test")
                print(pos)
                print(commentaire[pos].value)
                print("fin du test")
                pp.pprint(liste_xls[inc-1])
                liste_xls[inc-1].append(commentaire[pos].value)
            else:
                liste_xls[inc-1].append(std[inc_lettre+str(inc)].value)
           
        inc+=1
    print("\n\n\n\n")


    for  ligne in liste_xls:
        writer.writerow(ligne)

except sqlite3.Error as er:
    print 'er:', er.message


finally:
    if conn:
        conn.close()