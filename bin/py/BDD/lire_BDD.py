#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import sqlite3
import sys, getopt
import pprint
import csv
import os

pp = pprint.PrettyPrinter(indent=4)

conn = sqlite3.connect('sortie/BDD.db')
c = conn.cursor()

# param
#param = getopt.getopt(args)


param_site=sys.argv[1:]



try:

    c.execute("SELECT * FROM site")

    rows = c.fetchall()    

    pp.pprint(rows)
    if os.path.exists('resultat.csv'):
        os.remove('resultat.csv')
    fichier_csv = open('resultat.csv',"a")
    writer = csv.writer(fichier_csv,delimiter =';')
    writer.writerow(("url","404notfound","robot.txt","sitemap","description#title","semantique","commentaire","site_libelle"))

    
    for  ligne in rows:
            writer.writerow(ligne)

except sqlite3.Error as er:
    print 'er:', er.message


finally:
    if conn:
        conn.close()