#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import sqlite3
import sys, getopt
import pprint
pp = pprint.PrettyPrinter(indent=4)


def checkTableExists(dbcon, tablename):
    try:
        dbcur = dbcon.cursor()
        dbcur.execute("""
            SELECT COUNT(*)
            FROM information_schema.tables
            WHERE table_name = '{0}'
            """.format(tablename.replace('\'', '\'\'')))
        if dbcur.fetchone()[0] == 1:
            dbcur.close()
            return True

        dbcur.close()
        return False

    except sqlite3.OperationalError:
        dbcur.close()
        return True



conn = sqlite3.connect('sortie/BDD.db')
c = conn.cursor()

if( (checkTableExists(conn,"site")) ):
    c.execute("CREATE TABLE site("\
    "url varchar(128),"\
    "notfound  varchar(5),"\
    "robot_txt VARCHAR(5),"\
    "sitemap VARCHAR(5),"\
    "description_title VARCHAR(128),"\
    "semantique TEXT ,"\
    "commentaire TEXT,"\
    "site_libelle varchar(128) PRIMARY KEY  );")

if conn:
    conn.close()