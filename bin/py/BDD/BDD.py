#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import sqlite3
import sys, getopt
import pprint
pp = pprint.PrettyPrinter(indent=4)


def checkTableExists(dbcon, tablename):
    try:
        dbcur = dbcon.cursor()
        dbcur.execute("""
            SELECT COUNT(*)
            FROM information_schema.tables
            WHERE table_name = '{0}'
            """.format(tablename.replace('\'', '\'\'')))
        if dbcur.fetchone()[0] == 1:
            dbcur.close()
            return True

        dbcur.close()
        return False

    except sqlite3.OperationalError:
        dbcur.close()
        return True

conn = sqlite3.connect('sortie/BDD.db')
c = conn.cursor()

# param
#param = getopt.getopt(args)


param_site=sys.argv[1:]

#if( not(checkTableExists(conn,"site")) ):
c.execute("CREATE TABLE IF NOT EXISTS  site("\
"url varchar(128),"\
"notfound  varchar(5),"\
"robot_txt VARCHAR(5),"\
"sitemap VARCHAR(5),"\
"description_title VARCHAR(128),"\
"semantique TEXT ,"\
"commentaire TEXT,"\
"site_libelle varchar(128) PRIMARY KEY  );")

try:
    c.execute("SELECT * FROM site")
    rows = c.fetchall()    
    # pp.pprint(rows)

    for i in range(0,8):
        if len(param_site) == i:
            param_site.append("")

    requete_insertion = "REPLACE INTO site ("\
    "url,"\
    "notfound,"\
    "robot_txt,"\
    "sitemap,"\
    "description_title,"\
    "semantique,"\
    "commentaire,"\
    "site_libelle"\
    ") VALUES("\
    "'"+param_site[0]+"',"\
    "'"+param_site[1]+"',"\
    "'"+param_site[2]+"',"\
    "'"+param_site[3]+"',"\
    "'"+param_site[4]+"',"\
    "'"+param_site[5]+"',"\
    "'"+param_site[6]+"',"\
    "'"+param_site[7]+"'"\
    ");"

    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)
    # print(requete_insertion)

    c.execute(requete_insertion)
    conn.commit()

except sqlite3.Error as er:
    print 'er:', er.message


finally:
    if conn:
        conn.close()