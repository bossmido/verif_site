#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import  csv
import  os
import  re
import  pprint
import  json
import codecs
from    enum                    import Enum
import  subprocess
import pprint
pp = pprint.PrettyPrinter(indent=4)

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def json_load_byteified(file_handle):
    return _byteify(
        json.load(file_handle, object_hook=_byteify),
        ignore_dicts=True
    )

def json_loads_byteified(json_text):
    return _byteify(
        json.loads(json_text, object_hook=_byteify),
        ignore_dicts=True
    )

def _byteify(data, ignore_dicts = False):
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
            for key, value in data.iteritems()
        }
    # if it's anything else, return it in its original form
    return data


chaine_sortie ='['

command       = ['bash', '-c', 'source bin/config/chemin.sh && env']
command2      = ['bash', '-c', 'source bin/config/chemin_fichier.sh && env']

proc          = subprocess.Popen(command, stdout = subprocess.PIPE)
proc2         = subprocess.Popen(command2, stdout = subprocess.PIPE)


for line in proc.stdout:
  (key, _, value) = line.partition("=")
  os.environ[key] = value
proc.communicate()


for line in proc2.stdout:
  (key, _, value) = line.partition("=")
  os.environ[key] = value
proc2.communicate()

global GLOBAL_CHEMIN_CSV
global GLOBAL_BASE_SORTIE

GLOBAL_CHEMIN_CSV  = os.environ["CHEMIN_CSV"].replace("\n",'')
GLOBAL_BASE_SOURCE = os.environ["BASE_SOURCE"].replace("\n",'')
GLOBAL_BASE_SORTIE = os.environ["BASE_SORTIE"].replace("\n",'')

#print(GLOBAL_CHEMIN_CSV)
#print(GLOBAL_BASE_SOURCE )
sortie_csv_final   =GLOBAL_CHEMIN_CSV
#filename_csv      =  open(sortie_csv_final,"rw")
#filename_csv      = csv.DictReader(open(sortie_csv_final), ("url", "404notfound","robot.txt","sitemap","description#title","semantique","commentaire","site_libelle"))#,delimiter=';')
filename_csv       = csv.reader(open(sortie_csv_final),delimiter=";", lineterminator='\n')
donnees_de_site    = list(filename_csv)
nombre_ligne_csv   = len(donnees_de_site)

#pprint.pprint(filename_csv)
names_list  = open(sortie_csv_final).read().splitlines()
len_complet = len(names_list)
inc=0

chaine_fin=False
for row in names_list:
    liste_correct = row.split(';')
    if len(liste_correct)>1:
        #chaine_sortie = chaine_sortie + len(liste_correct)
        #chaine_sortie = chaine_sortie  +(liste_correct[1])
        chaine_sortie         = chaine_sortie  + "\n{"
        chaine_sortie         = chaine_sortie  + "\n\"url\" : \""+liste_correct[0].replace('"', '\\"')+"\",\n"
        if len(liste_correct)>1:
            sous_chaine_info_404= 0
            info_simple = liste_correct[1].replace('"', '\"').split(' ',2)
            #pp.pprint(info_simple)

            #traitement par partie
            moinsun = -1
            if RepresentsInt(info_simple[0]):
                moinsun = int(info_simple[0])
            else:
                moinsun = -1
            
            separateur = -10
            if len(info_simple)>1:
                if RepresentsInt(info_simple[1]):
                  separateur = int(info_simple[1])
                else:
                  separateur = -10
            else:
                separateur = -10
            nombre_erreur=0
            if len(info_simple)>2:
                if RepresentsInt(info_simple[2]):
                    nombre_erreur = int(info_simple[2])
                else:
                    nombre_erreur = 0
            else:
                nombre_erreur = 0
            
            if len(info_simple)>3:

                if len(info_simple[3])>3 :
                    tab_parse = info_simple[3].replace("[[","@@@@@@").replace("]]","@@@@@@").replace("uhttp","http").replace("[","[\"").replace("]","\"]").replace("[\"\\\"","[\"").replace("\"\"","\"")
                    #tab_parse = info_simple[3]

                    #pp.pprint(info_simple[3].replace("[u","[").replace("uhttp","http").replace("[","[\"").replace(",","\",\"").replace("]","\"]").replace("[\"\\\"","[\"").replace("\\\"\"]","\"]") )

                    print(tab_parse.decode('utf-8', 'ignore'))
                    liste_fichier_manquant =  json.loads(tab_parse.decode('utf-8', 'ignore'))
                   #liste_fichier_manquant =  json.loads(tab_parse)
                else:
                    #pp.pprint(info_simple[3])

                    liste_fichier_manquant=""
            else:
                liste_fichier_manquant=[]

            chaine_sortie=chaine_sortie+" \"404notfound\"  : {"
            chaine_sortie=chaine_sortie+"\n\"moins_un\" : "+str(moinsun)
            chaine_sortie=chaine_sortie+"\n,\"nombre_erreur\" : "+str(nombre_erreur)
            chaine_sortie=chaine_sortie+"\n,\"fichier\" : [ "
            inc_fichier=0
            for   fichier in liste_fichier_manquant:
                chaine_sortie=chaine_sortie+"\""+fichier+"\""
                if  ((inc_fichier+1) != len(liste_fichier_manquant)):
                    chaine_sortie=chaine_sortie+","
                inc_fichier = inc_fichier+1
            #chaine_sortie=chaine_sortie+"\n"
            chaine_sortie=chaine_sortie+"]\n}"
            #chaine_sortie     = chaine_sortie  + "\"404notfound\" : \""+liste_correct[1].replace('"', '\\"')+"\""
            if len(liste_correct)>2:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
        if len(liste_correct)>2:
            chaine_sortie     = chaine_sortie  + "\"robotnotfound\" : \""+liste_correct[2].replace('"', '\\"')+"\""
            if len(liste_correct)>3:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
            chaine_fin=False
        else:
            chaine_sortie     = chaine_sortie  +","
            chaine_fin=True
        if len(liste_correct)>3:
            chaine_sortie     = chaine_sortie  + "\"sitemap\" : \""+liste_correct[3].replace('"', '\\"')+"\""
            if len(liste_correct)>4:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
  
            chaine_fin=False
        else:
            chaine_sortie     = chaine_sortie  +","
            chaine_fin=True
        if len(liste_correct)>4:
            chaine_sortie     = chaine_sortie  + "\"description#title\" : \""+liste_correct[4].replace('"', '\\"')+"\""
            if len(liste_correct)>5:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
  
            chaine_fin=False
        else:
            chaine_sortie     = chaine_sortie  +","
            chaine_fin=True
        if len(liste_correct)>5:
            chaine_sortie     = chaine_sortie  + "\"semantique\" : \""+liste_correct[5].replace('"', '\\"')+"\""
            if len(liste_correct)>6:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
  
            chaine_fin=False
        else:
            chaine_sortie     = chaine_sortie  +","
            chaine_fin=True
        if len(liste_correct)>6:
            chaine_sortie     = chaine_sortie  + "\"commentaire\" : \""+liste_correct[6].replace('"', '\\"')+"\""
            if len(liste_correct)>7:
                chaine_sortie     = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
  
            chaine_fin=False
        else:
            chaine_sortie     = chaine_sortie  +","
            chaine_fin=True
        if len(liste_correct)>7:
            chaine_sortie = chaine_sortie  + "\"site_libelle\" : \""+liste_correct[7].replace('"', '\\"')+"\""
            if len(liste_correct)>8:
                chaine_sortie = chaine_sortie  + ","
            chaine_sortie     = chaine_sortie  + "\n"
  
            chaine_fin=False
        else:
            chaine_fin=True
            chaine_sortie     = chaine_sortie  +","
        chaine_sortie         = chaine_sortie  + "\n}"
        inc+=1

        if (len_complet) != (inc+2):

            chaine_sortie     = chaine_sortie  + ","


# try:
#     liste_correct=[]
#     empty_lines=0
#     for row in csv.reader(open(sortie_csv_final),delimiter=";", lineterminator='\n'):
#         if not line: 
#             empty_lines += 1 
#         liste_correct.append(row)
#         inc      = 1 + inc
#     for row in csv.reader(open(sortie_csv_final),delimiter=";", lineterminator='\n'):
#         if not line: 
#             empty_lines += 1 
#         liste_correct.append(row)
#         inc      = 1 + inc

#     inc=0
#     pp.pprint(liste_correct)
#     for row in liste_correct:
#if inc > 1:
            #if len(row)>0:
                #chaine_sortie = chaine_sortie + len(row)
#               chaine_sortie = chaine_sortie  +(row[1])
                # chaine_sortie     = chaine_sortie  + "\n{"
                # chaine_sortie     = chaine_sortie  + "\n'url' : '"+row[0]+"',\n"
                # if len(row)>1:
                #     chaine_sortie = chaine_sortie  + "'404notfound' : '"+row[1]+"',\n"
                # if len(row)>2:
                #     chaine_sortie = chaine_sortie  + "'robotnotfound' : '"+row[2]+"',\n"
                # if len(row)>3:
                #     chaine_sortie = chaine_sortie  + "'sitemap' : '"+row[3]+"',\n"
                # if len(row)>4:
                #     chaine_sortie = chaine_sortie  + "'description#title' : '"+row[4]+"',\n"
                # if len(row)>5:
                #     chaine_sortie = chaine_sortie  + "'semantique' : '"+row[5]+"',\n"
                # if len(row)>6:
                #     chaine_sortie = chaine_sortie  + "'commentaire' : '"+row[6]+"',\n"
                # if len(row)>7:
                #     chaine_sortie = chaine_sortie  + "'site_libelle' : '"+row[7]+"'"
                #chaine_sortie     = chaine_sortie  + "\n},"
        #inc=inc+1
# except Exception:
#     print(row)


chaine_sortie                     = chaine_sortie+']'
#pp.pprint(filename_csv)
#pprint.pprint(filename_csv.next())
#print(chaine_sortie)

obj_json_sortie = json.loads( codecs.encode(chaine_sortie,"ascii","ignore"))
sortie_verifie=json.dumps(obj_json_sortie, separators=(',', ':'))
print(sortie_verifie)

#print(chaine_sortie)

