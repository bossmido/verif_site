#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

import  csv
import  os
import  re
import  pprint
import  json
import sys

from    enum                    import Enum
import  subprocess
import pprint
pp = pprint.PrettyPrinter(indent=4)


def json_load_byteified(file_handle):
    return _byteify(
        json.load(file_handle, object_hook=_byteify),
        ignore_dicts=True
    )

def json_loads_byteified(json_text):
    return _byteify(
        json.loads(json_text, object_hook=_byteify),
        ignore_dicts=True
    )

def _byteify(data, ignore_dicts = False):
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
            for key, value in data.iteritems()
        }
    # if it's anything else, return it in its original form
    return data



def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

chaine_sortie ='['
command       = ['bash', '-c', 'source bin/config/chemin.sh && env']
command2      = ['bash', '-c', 'source bin/config/chemin_fichier.sh && env']
proc          = subprocess.Popen(command, stdout = subprocess.PIPE)
proc2         = subprocess.Popen(command2, stdout = subprocess.PIPE)


for line in proc.stdout:
  (key, _, value) = line.partition("=")
  os.environ[key] = value
proc.communicate()


for line in proc2.stdout:
  (key, _, value) = line.partition("=")
  os.environ[key] = value
proc2.communicate()

global GLOBAL_CHEMIN_CSV
global GLOBAL_BASE_SORTIE

GLOBAL_CHEMIN_CSV  = os.environ["CHEMIN_CSV"].replace("\n",'')
GLOBAL_BASE_SOURCE = os.environ["BASE_SOURCE"].replace("\n",'')
GLOBAL_BASE_SORTIE = os.environ["BASE_SORTIE"].replace("\n",'')

# if sys.argv[1] =="--tab":
#     sortie_csv_final   = "./"+GLOBAL_CHEMIN_CSV[:-4]+"_tab.json"
# else:
#     sortie_csv_final   = "./"+GLOBAL_CHEMIN_CSV[:-4]+".json"

sortie_csv_final   = "./"+GLOBAL_CHEMIN_CSV[:-4]+".json"

json_data          = open(sortie_csv_final)
data               = json_load_byteified(json_data)
sortie_json={}
inc        = 0
chaine_fin = False

liste_attribut = data[0].keys()
for objet in data:
   #pprint.pprint(objet)
    for attr,val in objet.iteritems():
        if not(attr  in sortie_json.keys()):
            pass
            sortie_json[attr] =[]
            sortie_json[attr].append(val)
        else:
            pass
            liste_intermediaire = [val]
            sortie_json[attr]=sortie_json[attr] + liste_intermediaire
#pprint.pprint(sortie_json)
#obj_json_sortie = json.loads(chaine_sortie)
#print(str(sortie_json))
sortie_verifie=json.dumps(sortie_json, separators=(',', ':')).replace("'","\"").replace("\"u\"","\"").replace("\"\"","\"")

print(sortie_verifie)