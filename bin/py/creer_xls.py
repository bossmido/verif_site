#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from    openpyxl                import *
from    openpyxl                import worksheet
from    openpyxl.drawing.fill   import PatternFillProperties, ColorChoice
from    openpyxl.styles         import PatternFill, Border, Side, Alignment, Protection, Font, Color
from    openpyxl.styles         import Alignment

import  csv
import  os
from    pprint                  import pprint
import  re
from    pathlib                 import Path
import  pprint
from    enum                    import Enum

pp = pprint.PrettyPrinter(indent=4)


def set_border(ws, cell_range):
    border = Border(left=Side(border_style='thin', color='f1f1f1'),
                right=Side(border_style='thin', color='f1f1f1'),
                top=Side(border_style='thin', color='f1f1f1'),
                bottom=Side(border_style='thin', color='f1f1f1'))

    rows =ws[cell_range]
    for row in rows:
        for cell in row:
            cell.border = border


def test_semantique():
    print()

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def style_range(ws, cell_range,site, style=None):
    start_cell, end_cell = cell_range.split(':')
    start_coord = coordinate_from_string(start_cell)
    start_row = start_coord[1]
    start_col = column_index_from_string(start_coord[0])
    end_coord = coordinate_from_string(end_cell)
    end_row = end_coord[1]
    end_col = column_index_from_string(end_coord[0])

    for row in range(start_row, end_row + 1):
        for col_idx in range(start_col, end_col + 1):
            col = get_column_letter(col_idx)
            ws.cell('%s%s' % (col, row)).style = style


class Test(Enum):
    erreur_404 = 1
    sitemap = 2
    robot = 3
    title = 4
    semantique = 5
    rien = 6
    semantique_complexe = 7

compteur_erreur_sitemap=0
liste_global_site_erreur= {'erreur_404':' ','sitemap':' ','robot':' ','title':' ','semantique' : ' ','semantique_complexe' : ' '}
def affecter_val(cell,destination,site,test=Test.rien):

    std=wb.get_sheet_by_name('resultat_csv')
    cell = std[cell]

    std1=wb.get_sheet_by_name('resultat_affichage')
    cell2 = std1[destination]

    bleu = PatternFill(start_color='0057c9', end_color='0057c9', fill_type='solid')
    rouge_ref = PatternFill(start_color='c90000', end_color='c90000', fill_type='solid')
    rouge = PatternFill(start_color='c90000', end_color='c90000', fill_type='solid')

    orange = PatternFill(start_color='ffa500', end_color='ffa500', fill_type='solid')

    if(cell.value != None ):
        tab_parse=cell.value .split(" ")
        #if(not RepresentsInt(tab_parse[0])):
        if((test == Test.semantique_complexe) and (cell.value != None) and (len(cell.value)>10 ) ):
                    liste_global_site_erreur['semantique_complexe'] += " "+site 
        pp.pprint((tab_parse))
        print('\n\n')
        pp.pprint((tab_parse))
        print('\n\n')
        pp.pprint((tab_parse))
        print('\n\n')

        pp.pprint((tab_parse))

        if(len(tab_parse)>=1):
            if(len(tab_parse)==1 and not RepresentsInt(tab_parse[0])):
                if(cell.value == "KO" ):
                    cell.fill = cell2.fill = rouge_ref
                    if(test==Test.sitemap):
                        global compteur_erreur_sitemap
                        liste_global_site_erreur['sitemap'] += " "+site 
                        compteur_erreur_sitemap+=1
                    if(test == Test.erreur_404):
                        liste_global_site_erreur['erreur_404'] += " "+site 
                    if(test == Test.robot):
                        liste_global_site_erreur['robot'] += " "+site 
                    if(test == Test.title):
                        liste_global_site_erreur['title'] += " "+site 
                else:
                 
                    cell.fill  = cell2.fill= bleu
            else:
                if(len(tab_parse)>1):
                    if(int(tab_parse[2])==(-1)):
                        cell.fill = cell2.fill = rouge
                        if(test == Test.erreur_404):
                            liste_position_erreur.append(destination)
                            liste_global_site_erreur['erreur_404'] += " "+site
                    if(int(tab_parse[2])==(-2)):
                        cell.fill = cell2.fill = orange
                        if(test == Test.erreur_404):
                            liste_position_erreur.append(destination)
                            liste_global_site_erreur['erreur_404'] += " "+site         
                    if(int(tab_parse[2])>=1):
                        if(test == Test.erreur_404):
                            liste_global_site_erreur['erreur_404'] += " "+site 
                        liste_position_erreur.append(destination)
                        liste_nombre_erreur.append(int(tab_parse[2]))
                        cell.fill = cell2.fill = rouge_ref
                    if(int(tab_parse[2])==0):
                        cell.value = "pas d erreur"
    else:
        cell.value = "VIDE";

#cwd = os.path.dirname(os.path.realpath(__file__))
#os.chdir("sortie")
chemin_actuel = os.getcwd()
# os.chdir("..")
# os.chdir("..")
os.chdir("sortie")

def extract_basename(path):
  """Extracts basename of a given path. Should Work with any OS Path on any OS"""
  basename = re.search(r'[^\\/]+(?=[\\/]?$)', path)
  if basename:
    return basename.group(0)

redFill = ""
thin_border = Border(left=Side(style='thin'), 
                     right=Side(style='thin'), 
                     top=Side(style='thin'), 
                     bottom=Side(style='thin'))

filename_xlsx_c =  "resultat.xlsx"
filename_xls_c  =  "resultat.xls"
filename_csv_c  =  "resultat.csv"
if not os.path.exists(filename_xlsx_c):
    os.mknod(filename_xlsx_c)
if not os.path.exists(filename_xls_c):
    os.mknod(filename_xls_c)
if not os.path.exists(filename_csv_c):
    os.mknod(filename_csv_c)
filename_xlsx = open(filename_xlsx_c,"rw")
fichier_xls   = open(filename_xls_c,"rw")
fichier_csv   = open(filename_csv_c,"rw")


# if os.path.exists(filename_xlsx_c):
#     os.remove(filename_xlsx_c);
# if os.path.exists(filename_xls_c):
#     os.remove(filename_xls_c);
# if os.path.exists(filename_csv_c):
#     os.remove(filename_csv_c);


csvreader        = csv.reader(fichier_csv,delimiter=';')
donnees_de_site  = list(csvreader)
nombre_ligne_csv = len(donnees_de_site)

wb  = Workbook()
wb.get_sheet_names()
std = wb.get_sheet_by_name('Sheet')
wb.remove_sheet(std)


# debut de travail

ws = wb.create_sheet("resultat_csv")

for i  in range(ord("A")-ord("A"),ord("F")-ord("A")):
    ws.column_dimensions[str(chr(ord("A")+i))].width = 40

wb.active = 0
#wb = openpyxl.load_workbook('resultat.xls')

inc=0
#ws = wb.create_sheet("cache")

row=()
try:
    for row in donnees_de_site:
        ws_tmp = wb.get_sheet_by_name('resultat_csv')
        inc    = 1+inc
        ws_tmp.append(row)
        #ws_tmp["A"+str(inc+2)].value="=cache!G"+str(inc)
        #ws_tmp["G"+str(inc+2)].value="C"+str(((i-2)*mod+1)+5+decalage)
     #   cache=wb.get_sheet_by_name('cache')
      #  if(len(row)>0 and row[6]==""):#row commentaire
       #     cache["A"+str(inc)].value="rien"
        #    cache.column_dimensions['A'].visible = False
except Exception:
    print(row)

#wb.worksheets[0].max_row = 10
#wb.worksheets[0].max_column = (inc*(7+1)+4+50+inc)

ws['G1'].value="Commentaire"
wb.save("resultat.xlsx")
#wb = load_workbook('resultat.xlsx')
ws = wb.create_sheet("resultat_affichage")
wb.active = 1
# automatisation creation liste critere 
#ws['A1'].fill = redFill
ws['A1'].value = "verification de la SEO"
fontObj       = Font(size=24, italic=True)
ws['A1'].font = fontObj
#ws['A1'].style.font.color = Color(Color.BLUE) 


ws.column_dimensions['A'].width = 45
ws.column_dimensions['C'].width = 45

mod                         = 8
decalage                    = 2
i                           = 0
nombre_site_erreur          = 0
dictionnaire_toute_position = {}

liste_position_erreur       = list()
nombre_total_erreur         = 0
liste_nombre_erreur         = list()

# debut worsheet d'affichage
for i in range(2,nombre_ligne_csv+2): 
    val       = "A"+str(((i-2)*mod+1)+1+decalage)
    debut_a_b = "B"+str(((i-2)*mod+1)+1+decalage)
    fin_a_c   = "C"+str(((i-2)*mod+1)+1+decalage)
    val16     = "C"+str(((i-2)*mod+1)+5+decalage)
    val_fin   = "C"+str(((i-2)*mod+1)+7+decalage)
    ws.merge_cells(debut_a_b+':'+fin_a_c)
    HeaderFill = PatternFill(start_color='002b43', end_color='002b43', fill_type='solid')

    for row in ws[val:val_fin]:
        for cell in row:
            cell.fill = HeaderFill
    
    ws_copie      = wb["resultat_csv"]
    res_site      = ws_copie["A"+str(i+1)]
    vrai_res_site = ws_copie["H"+str(i+1)]
    #print("A"+str(1+i))
   
 
    ws[val].border = thin_border
    if(res_site.value == None):
        res_site.value = ""
    dictionnaire_toute_position[res_site.value] = val
    rgb=[255,0,0]
    color_string="".join([str(hex(j))[2:].upper().rjust(2, "0") for j in rgb])
    ws[val].fill=PatternFill(fill_type="solid", start_color='FF' + color_string, end_color='FF' + color_string)
    #ws[val].value = '=HYPERLINK( "'+res_site.value+'" ; "Site : '+res_site.value+' ") '
    if(res_site.value == None):
        res_site.value=""
    if(vrai_res_site.value == None):
        vrai_res_site.value=""
    ws[val].value     = '=HYPERLINK("'+vrai_res_site.value+'","'+res_site.value+'")'
    
    ws[val].style     = 'Hyperlink'
    ws[val].hyperlink = ("http://"+res_site.value+"")
    val2              = "A"+str(abs((i-2)*mod+1)+2+decalage)
    objval2           = ws[val2]
    ws[val2].value    = "erreur 404"
    
    val3              = "A"+str(abs((i-2)*mod+1)+3+decalage)
    objval3           = ws[val3]
    ws[val3].value    = "sitemap"
    
    val4              = "A"+str(abs(((i-2)*mod+1)+4+decalage))
    ws[val4].border   = thin_border
    ws[val4].value    = "robot.txt"
    
    val5              = "A"+str(abs((i-2)*mod+1)+5+decalage)
    ws[val5].value    = "balise <title> et attribut description"
    
    val6 = "A"+str(abs((i-2)*mod+1)+7+decalage)
    ws[val6].value = "semantique"
    
    
    val7 = "A"+str(abs((i-2)*mod+1)+6+decalage)
    ws[val7].value = "Commentaire"
    #print(abs((i-2)*mod+1)+6+decalage)
    ws.row_dimensions[abs((i-2)*mod+1)+6+decalage].height = 300
    gris_fond = PatternFill(start_color='d3d3d3', end_color='d3d3d3', fill_type='solid')
    ws["C"+str(abs((i-2)*mod+1)+6+decalage)].fill = gris_fond


    #print("=resultat_csv!"+str(chr(ord("A")+1))+str(i))

    val12 =  "C"+str(((i-2)*mod+1)+2+decalage)
    #404
    ws[val12].value   = "=resultat_csv!$"+str(chr(ord("A")+1))+str(i+1)
    orange = PatternFill(start_color='ffa500', end_color='ffa500', fill_type='solid')
    rouge = PatternFill(start_color='ffa000', end_color='ff0000', fill_type='solid')

    if(ws_copie[str(chr(ord("A")+1))+str(i+1)].value == None):
        ws_copie[str(chr(ord("A")+1))+str(i+1)].value = "0"
    if(RepresentsInt(ws_copie[str(chr(ord("A")+1))+str(i+1)].value)):
        if(int(ws_copie[str(chr(ord("A")+1))+str(i+1)].value) == -1):
            #ws_copie[str(chr(ord("A")+1))+str(i+1)].value = "site inaccessible" 
            ws[val12].value   = "site inaccessible"
            ws[val12].fill    = rouge
            ws_copie[str(chr(ord("A")+1))+str(i+1)].fill  = rouge
        if(int(ws_copie[str(chr(ord("A")+1))+str(i+1)].value) == -2):
            #ws_copie[str(chr(ord("A")+1))+str(i+1)].value = "site inaccessible" 
            ws[val12].value   = "site peut-etre inaccessible"
            ws[val12].fill    = rouge
            ws_copie[str(chr(ord("A")+1))+str(i+1)].fill  = orange

    affecter_val( str(chr(ord("A")+1))+str(i+1),val12,res_site.value,Test.erreur_404)

    val13="C"+str(((i-2)*mod+1)+3+decalage)
    ws[val13].value = "=resultat_csv!"+str(chr(ord("A")+2))+str(i+1)
    affecter_val( str(chr(ord("A")+2))+str(i+1),val13,res_site.value,Test.sitemap)

    val14="C"+str(((i-2)*mod+1)+4+decalage)
    ws[val14].value = "=resultat_csv!"+str(chr(ord("A")+3))+str(i+1)
    affecter_val( str(chr(ord("A")+3))+str(i+1),val14,res_site.value,Test.robot)

    val15="C"+str(((i-2)*mod+1)+5+decalage)
    ws[val15].value = "=resultat_csv!"+str(chr(ord("A")+4))+str(i+1)
    affecter_val( str(chr(ord("A")+4))+str(i+1),val15,res_site.value,Test.title)

    #!!!!
    ws[val16].value =  "=resultat_csv!"+str(chr(ord("A")+4))+str(i+1)
    affecter_val( str(chr(ord("A")+4))+str(i+1),val16,res_site.value,Test.semantique)

    val17="C"+str(((i-2)*mod+1)+7+decalage)
    ws[val17].value =  "=resultat_csv!"+str(chr(ord("A")+5))+str(i+1)
    affecter_val( str(chr(ord("A")+5))+str(i+1),val17,res_site.value,Test.semantique_complexe)
####################################################################
    #changer couleur 404
    val17="C"+str(((i-2)*mod+1)+6+decalage)
    val17_libelle="A"+str(((i-2)*mod+1)+6+decalage)

    # print val17
    # print (chr(ord("A")+5))+str(i+1)
    ws[val17].alignment =   ws[val17_libelle].alignment =  Alignment(vertical="top",wrapText=True)

    if(ws[val17].value  == None):
        #ws[val17].value =  '=IF(INDIRECT("resultat_csv.'+'H'+str(i+1)+'"),"commentaire a faire",INDIRECT("resultat_csv.'+'H'+str(i+1)+'"))'
        #ws[val17].value =  '=INDIRECT("commentaire ","resultat_csv.'+'H'+str(i+1)+'")'
        ws[val17].value =  ws_copie[str(chr(ord("A")+6))+str(i+1)].value
        
    ws_copie[ str(chr(ord("A")+6))+str(i+1)].value = '=resultat_affichage!$'+val17
    test_semantique()

# fin automatisation liste critere
####################################################################
ws = wb.create_sheet("resultat_statistique")
ws.column_dimensions["A"].width = 150
ws.column_dimensions["B"].width = 150
ws.column_dimensions["C"].width = 150


k=0
pos_total_rangee =  1
position_debut_total= val_total_libelle = "A"+str(pos_total_rangee)
fontObj = Font(size=24, italic=True)
ws[val_total_libelle].font = fontObj
ws.row_dimensions[pos_total_rangee].height = 40
ws.row_dimensions[pos_total_rangee].alignment =  Alignment(vertical="top",wrapText=True)

ws[val_total_libelle].value = "TOTAL 404 :"
k+=1
val_total_libelle = "A"+str(decalage+k)

ws[val_total_libelle].value = str(len(liste_position_erreur))+" sites avec liens morts "
k+=2

####################################################################
pos_total_lien_mort_rangee = decalage+k
val_total_libelle = "A"+str(pos_total_lien_mort_rangee)
fontObj = Font(size=24, italic=True)
ws[val_total_libelle].font = fontObj
ws.row_dimensions[pos_total_lien_mort_rangee].height = 40

ws[val_total_libelle].value = "TOTAL de lien mort total:"
k+=1
lien_mort_total=0
for valeur_courante in  (liste_nombre_erreur):
    lien_mort_total += valeur_courante
#var_dump(liste_nombre_erreur)
val_total_libelle = "A"+str(decalage+k)
ws[val_total_libelle].value = str(lien_mort_total)+" liens morts "

####################################################################
k+=2
pos_total_sitemap  = decalage+k
val_total_libelle = "A"+str(pos_total_sitemap)
fontObj = Font(size=24, italic=True)
ws[val_total_libelle].font = fontObj
ws[val_total_libelle].value = "TOTAL sitemap+robot.txt(confondu) :"
ws.row_dimensions[pos_total_sitemap].height = 55

k+=1
val_total_libelle = "A"+str(decalage+k)
#global compteur_erreur_sitemap
ws[val_total_libelle].value = str(compteur_erreur_sitemap)+""


####################################################################

k+=2
pos_liste_complete_site =decalage+k
val_total_libelle = "A"+str(pos_liste_complete_site)
fontObj = Font(size=24, italic=True)
ws.row_dimensions[pos_liste_complete_site].height = 40

ws[val_total_libelle].font = fontObj
ws[val_total_libelle].value = "LISTE COMPLETE DES SITES :"
k+=1
for site, position_courante in (dictionnaire_toute_position.iteritems()):
    val_total_libelle = "A"+str((decalage+k))
    val_total_libelle2 = "B"+str((decalage+k))
    ws[val_total_libelle].value = ''+site
    ws[val_total_libelle2].value = '=HYPERLINK("resultat_affichage!$'+position_courante+'","LIEN")'
    ws[val_total_libelle2].style = 'Hyperlink'
   # ws[val_total_libelle2].hyperlink = ("http://"+position_courante.value+"")
    k+=1

####################################################################

k+=2
pos_liste_complete_site =decalage+k
val_total_libelle = "A"+str(pos_liste_complete_site)
fontObj = Font(size=24, italic=True)
ws.row_dimensions[pos_liste_complete_site].height = 60
ws[val_total_libelle].font = fontObj
ws[val_total_libelle].value = "LISTE COMPLETE DES \n SITES AVEC ERREURS:"
k+=1

for probleme, sites in (liste_global_site_erreur.iteritems()):
    val_total_libelle2 = "B"+str((decalage+k))

    val_total_libelle = "A"+str((decalage+k))
    ws[val_total_libelle].value = ''+sites
    bleu_fond = PatternFill(start_color='0000ff', end_color='0000ff', fill_type='solid')
    ws[val_total_libelle].fill = bleu_fond
    #ws[val_total_libelle].value = probleme
    ws[val_total_libelle2].value = probleme
    k+=1

####################################################################
# position_debut_total
# val_total_libelle2

    # for row in ws[position_debut_total:val_total_libelle2]:
    #     for cell in row:
    #         cell.border = thin_border
set_border(ws,position_debut_total+':'+val_total_libelle2)

####################################################################

#pprint(globals())

wb.save("resultat.xlsx")

########################################################################################

# import openpyxl
# import zipfile
# from shutil import copyfile
# from shutil import rmtree
# import os

# PAD = os.getcwd()

# wb = openpyxl.load_workbook('script.xlsm')

# ws = wb.worksheets[0]
# ws.cell(row=2, column=3).value = 'Edited'

# wb.save('resultat.xlsx')


# with zipfile.ZipFile('script.xlsm', 'r') as z:
#     z.extractall('./xlsm/')

# with zipfile.ZipFile('resultat.xlsx', 'r') as z:
#     z.extractall('./xlsx/')

# copyfile('./xlsm/[Content_Types].xml','./xlsx/[Content_Types].xml')
# copyfile('./xlsm/xl/_rels/workbook.xml.rels','./xlsx/xl/_rels/workbook.xml.rels')
# copyfile('./xlsm/xl/vbaProject.bin','./xlsx/xl/vbaProject.bin')

# z = zipfile.ZipFile('tmp.zip', 'w')

# os.chdir('./xlsx')

# for root, dirs, files in os.walk('./'):
#         for file in files:
#             z.write(os.path.join(root, file))
# z.close()

# #clean
# os.chdir(PAD)
# rmtree('./xlsm/')
# rmtree('./xlsx/')
# os.remove('./resultat.xlsx')
# os.rename('tmp.zip', 'resultat.xlsm')

#######################################################################################



# from subprocess import *
# import os;
# a='ssconvert --export-type="Gnumeric_Excel:excel_dsf" "./resultat.xlsx" "./resultat.xls"'

#out=Popen(a,stdout=PIPE)
#(sout,serr)=out.communicate()
#exec(a)

#os.system(a);

#wb2.save("resultat.xls")


print("\n\n\n\n\n\n")


pp.pprint(liste_global_site_erreur)

os.chdir(chemin_actuel)