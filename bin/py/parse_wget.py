#!/usr/bin/python2

import os
import re
import sys
import pprint 
import json
import codecs

if __name__ == '__main__':

    pp = pprint.PrettyPrinter(indent=4)

    param_site=sys.argv[1:]


    file_object  =  codecs.open(param_site[0],encoding='utf8', mode="r")
    append_write=""
    if os.path.exists(param_site[1]):
        append_write = 'a' 
    else:
        append_write = 'w' 
    file_object_sortie  = codecs.open(param_site[1], encoding='utf-8',mode=append_write)

    log_lines = file_object.read()


    time_and_url_pat = re.compile(r'--(\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2})--[\ ]{2}(.*|(\S)*|(.|\n){0,200}|(\s)*) 404 (.*|(\S)*|(\s)*)',re.MULTILINE | re.IGNORECASE )
    ip_pat = re.compile(r'Connecting to.*\|(.*?)\|')
    time_and_url_list = time_and_url_pat.findall(log_lines)


    ip_list = ip_pat.findall(log_lines)

    all_data = [(t, u, i) for (t, u), i in zip(time_and_url_list, ip_list)]

    all_data += "test"


    sortie_str =""
    sortie_list=[]
    # pp.pprint(time_and_url_list[0][1])
    # print("")
    # print("")
    if len(time_and_url_list)>0:
        sortie_str += "["
    inc_resultat=0
    for t in time_and_url_list:
        inc=0

        if inc_resultat == 0 : 
            if len(time_and_url_list)>0:
                sortie_str += "["
        else:
            sortie_str += ",["
        for val in t:
            if val != "" :
                if inc == 1  : 
                    if inc==(len(t)-1) : 
                        if t[inc] != "" or len(t[inc]) >0 :
                            sortie_str += "\n,"+t[inc]
                            sortie_list.append(t[inc])

                #        sortie_str += "\n"+t[inc].split("\n")[1]
                    else:
                        if (t[inc].split("\n")[0])!= "" or len(t[inc].split("\n")[0]) >0 :
                            sortie_str += "\n"+t[inc].split("\n")[0]
                            sortie_list.append(t[inc].split("\n")[0])
                inc = inc+1
                passer=True;
        if len(time_and_url_list)>0:
            sortie_str += "]"
        inc_resultat = inc_resultat + 1
    if len(time_and_url_list)>0:
        sortie_str += "]"
    passer=False
    # if(time_and_url_list != None ):
    #     if( len(time_and_url_list) > 0 ):
    #         if(len(time_and_url_list[inc_resultat]) > 0 ):
    #             if(time_and_url_list[inc_resultat][1] != None):
    #                 passer=True;


    if isinstance(sortie_str,str):
        sortie_str = sortie_str.decode("ascii","ingore")
        
    if isinstance(sortie_str,unicode):
        sortie_str  = codecs.encode(sortie_str,"ascii","ignore")
   
    #print(sortie_str)
    pprint.pprint(sortie_list)
    file_object_sortie.write(sortie_str)
    if(passer):
        sys.exit( time_and_url_list[0][1] )
    else:
        sys.exit("")
    ##test       
    file_object.close()
    file_object_sortie.close()