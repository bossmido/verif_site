#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from enum import Enum


class TestStr:
    """Class TestStr
    """

    # Attributes:
    __name__= "TestStr"   
    
    val=None
    liste_str={
    "1":"erreur_404",
    "2":"sitemap",
    "3":"robot",
    "4":"title",
    "5":"semantique",
    "6":"rien",
    "7":"semantique_complexe"
    }

    # Operations
    def __init__(self,val=0):
        self.val=val

    def to_str(self):
        for cle,courant in self.liste_str.items():
            if self.val==int(cle):
                return courant
    
    def __repr__(self):
        return self.to_str()
    def __str__(self):
        return self.to_str()

    def val(self):
        return self.val
            