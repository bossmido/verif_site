
#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys 
import  os
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    from RefCSV             import RefCSV
    from TableauGenerique   import TableauGenerique
    from Direction          import Direction

else:
    from V2.RefCSV             import RefCSV
    from V2.TableauGenerique   import TableauGenerique
    from V2.Direction          import Direction

class TableauCSV(TableauGenerique): ### implementer dans le Tableau generique
    """Class TableauCSV
    """
    # Attributes:
    
    # Operations

    def __init__(self,x="A",y=1,feuille_nom=None,direction=None,liste_taille=(),type_entree=None):
        liste_taille=(50,50,50,50,50,50,50,50)
        type_entree=Direction.haut_bas

        super(TableauCSV,self).__init__(x,y,feuille_nom,direction,liste_taille,type_entree)