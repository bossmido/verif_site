#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import codecs
from    openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, Color
from    openpyxl.styles import Color, Fill
from    openpyxl        import worksheet
from    openpyxl.cell   import Cell




import sys 
import  os
from    os.path     import dirname, basename, isfile

if   sys.version_info.major   == 2:
    from    MonStyle        import MonStyle
    from    Dessinable      import Dessinable
    from    Position        import Position
    import  global_var

    from   Test            import Test
    from   Couleur         import Couleur


else:
    from    V2.MonStyle        import MonStyle
    from    V2.Dessinable      import Dessinable
    from    V2.Position        import Position
    import  V2.global_var

    from    V2.Test            import Test
    from    V2.Couleur         import Couleur



class Cellule(Dessinable):
    """Class Cellule
    """
    # Attributes:
    position_base       = None
    position_relative   = None  # (Position) 
    objet_cell          = None  # (Cell) 
    feuille_ou_ecrire   = None
    #########################################
    transformation      = None
    couleur             = None
    fond                = None
    border              = None
    # Operations
    def __init__(self,feuille_ou_ecrire="",position_base=None,position_relative=None,transformation=None,couleur=None,fond=None,border=None):
        if(position_base == None):
            self.position_base      = Position()
        else:
           self.position_base       = position_base
        if(position_relative == None):
            self.position_relative  = position_base
        else:
            self.position_relative  = position_relative
        
        if(position_relative == position_base):
            self.position_relative  = position_base
            self.position_base      = position_base

        self.feuille_ou_ecrire      = feuille_ou_ecrire
        self.transformation         = transformation
        self.couleur                = couleur
        self.fond                   = fond
        self.border                 = border
        ws_tmp                      = global_var.WORKBOOK_GLOBAL.get_sheet_by_name(feuille_ou_ecrire)        
        self.cell                   = ws_tmp[(position_base+position_relative).__str__()]

        self.objet_cell             = ws_tmp[((position_base+position_relative)+2).__str__()]
        # self.objet_cell.page_setup.fitToWidth = 1

    # def val(val):
    #     self.objet_cell.value = val

    # def  __getitem__(self,key):
    #     return self.objet_cell[key]
    #     if(isinstance(key,Position)):
    #         return self.objet_cell[key.__str__()]
    #     else
    #         return self.objet_cell[key] 



    # def  __setitem__(self, key, item):
    #     if(isinstance(key,Position)):
    #         self.objet_cell[key.__str__()] = val
    #     else
    #         self.objet_cell[key] = val

    def set_val(self,value_param):
        if value_param == None:
            return ""
        if isinstance(value_param,Position):
            value_param = value_param.__str__()
        if isinstance(value_param,str):
            #self.objet_cell.value = value_param.decode('utf-8').encode('latin-1')
            #value_param = codecs.decode(value_param,"utf-8","backslashreplace")
            #value_param = codecs.encode(value_param,"utf-8","backslashreplace")
            self.objet_cell.value = value_param.decode("latin-1")
            return 

        if isinstance(value_param,unicode):
            value_param  = codecs.encode(value_param,"utf-8","backslashreplace")
            self.objet_cell.value =  value_param
            return 

    def set_lien(self,libelle,lien):
        if libelle == None:
            return ""
        if isinstance(libelle,Position):
            libelle = libelle.__str__()
        #if self.objet_cell != None:
        self.objet_cell.value = u'=HYPERLINK( "'+libelle+' ";"'+lien+' " ) '
        self.objet_cell.font.color.rgb = "FF000080"

    def set_alignement(self,align="top"):
        self.objet_cell.alignment = Alignment(vertical=align,wrapText=True)
    
    def get_val(self):
        #if self.objet_cell != None:
        return self.objet_cell.value
        #else:
        #    return ""
    def set_couleur(self,couleur=Couleur.bleu):
        # ws_tmp = global_var.WORKBOOK_GLOBAL.get_sheet_by_name(self.feuille_ou_ecrire)        
        self.objet_cell.fill=PatternFill(fill_type="solid", start_color=couleur, end_color=couleur)

    def set_font(self,taille=19):
            fontObj = Font(size=taille, italic=True)
            self.objet_cell.font = fontObj

    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0):

        if(border):
            thin_border = Border(left=Side(style='thin'), 
                         right=Side(style='thin'), 
                         top=Side(style='thin'), 
                         bottom=Side(style='thin'))
            self.objet_Cell.border = thin_border
            self.objet_cell.fill=PatternFill(fill_type="solid", start_color=fond, end_color=fond)
            fontObj = Font(size=taille, italic=transformation)
            self.objet_Cell.font = fontObj
            self.objet_cell.font.color = couleur #Color.GREEN

    def dessiner_cellule(self,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,type_cellule=Test.rien):
        self.set_alignement()
        pass