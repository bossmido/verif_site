#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-


# all .pyx files in a folder
from distutils.core import setup,Extension
from Cython.Build import cythonize
import os, sys
# from Cython.Disutils import build_ext

import cython

import openpyxl
import pyximport
# pyximport.install(setup_args={"script_args":["--compiler=mingw32"],
#                               "include_dirs":openpyxl.get_include()},
#                   reload_support=True)


try:
  os.environ["LD_LIBRARY_PATH"] += "/usr/local/lib/python2.7/dist-packages/tablib/packages/openpyxl/"
except KeyError:
  os.environ["LD_LIBRARY_PATH"] ="/usr/local/lib/python2.7/dist-packages/tablib/packages/openpyxl/"

if '--use-cython' in sys.argv:
    USE_CYTHON = True
    sys.argv.remove('--use-cython')
else:
    USE_CYTHON = False
ext = '.py' if USE_CYTHON else '.cpp'

extensions = Extension(name="verif_seo", 
    sources=


    [
    # "./openpyxl/shared/*.py",
    # "./openpyxl/reader/*.py",
    # "./openpyxl/writer/*.py",
    # "./openpyxl/*.py",
    "*.py"
    ],
    language='c',
    #extra_compile_args = ['-std=gnu11'],
    #extra_link_args=['-std=gnu11'],
    extra_compile_args = ["-fopenmp","-O3"],
    extra_link_args=["-L/usr/local/lib/python2.7/dist-packages/tablib/packages/openpyxl/"],
    build_dir="/home/thi/build"
                    )

setup(
  name = 'verif_seo',
  ext_modules = cythonize(extensions),
  cmdclass={'build_ext': build_ext})#,
  #include_dirs = [np.get_include()]
#)


# extensions = [Extension("src.python.cython_utils",
#                         ["*."+ext],
#                         language='c++',
#                         include_dirs=['src/cpp/'])

# setup(
#     ext_modules = [cythonize(extensions)]
# )