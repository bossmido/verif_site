#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys 
from    os.path     import dirname, basename, isfile

from    openpyxl.styles import Font, Color 
from    openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, Color


if   sys.version_info.major  == 2:
    from   TableauGenerique import TableauGenerique
    from   TableauSiteListe import TableauSiteListe
    from   RefCSV          import RefCSV  
    from   Direction       import Direction
    import global_var 
    import monenv

else:
    from    V2.TableauGenerique import TableauGenerique
    from    V2.TableauSiteListe import TableauSiteListe
    from    V2.RefCSV          import RefCSV  
    from    V2.Direction       import Direction
    import  V2.global_var 
    import  V2.monenv


class TableauStat(TableauGenerique):
    """Class TableauStat
    """
    # Attributes:
    worksheet=None
    # Operations

    def __init__(self,x="A",y=1,feuille_nom="",nombre_ligne=0,direction=None,liste_taille=(),type_entree=Direction.gauche_droite):
        liste_taille=()
        liste_taille_ligne=[]
        for val in liste_taille:
            liste_taille_ligne.append(150)
        type_entree=Direction.gauche_droite
        super(TableauStat,self).__init__(x,y,feuille_nom,direction,liste_taille,type_entree,nombre_ligne=nombre_ligne)
        # for rangee in RefCSV.charger(monenv.GLOBAL_CHEMIN_CSV):
        #     self.liste_site.append(TableauSite(feuille_nom))
        self.changer_taille_colonne((60,60,60))

    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        global titre_tableau_stat
        global espacement_tableau_stat
        global liste_nombre_erreur
        # inc=0
        # inc_indice=0


        # position_courante = Position("A",3)
        # cellule_titre = CelluleLibelle(feuille_ou_ecrire,Position("A",1))
        # cellule_titre.value = titre_feuille_resultat
        # #position_courante = Position("A",3)
        # for rangee in liste_site:
        #     self.rangee.dessiner(self.feuille_nom,position_courante+inc,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=inc_indice):
        #     inc+=inc+global_var.espacement_tableau_stat
        
        ####################################################################
        if(self.worksheet == None):
            raise Exception("tableau Stat : pas de feuille d'excel !")
        k=2
        pos_total_rangee =  1
        position_debut_total = val_total_libelle = "A" + str(pos_total_rangee)
        fontObj = Font(size=24, italic=True)

        self.worksheet[val_total_libelle].font = fontObj
        self.worksheet.row_dimensions[pos_total_rangee].height = 40
        self.worksheet.row_dimensions[pos_total_rangee].alignment =  Alignment(vertical="top",wrapText=True)

        self.worksheet[val_total_libelle].value = "NOMBRE SITE AVEC  404 :"
        val_total_libelle = "A"+str(1+k)

        self.worksheet[val_total_libelle].value = str(len(global_var.liste_position_erreur))+" sites avec liens morts "
        k+=global_var.espacement_tableau_stat

        ####################################################################
        pos_total_lien_mort_rangee = global_var.espacement_tableau_stat+k
        val_total_libelle = "A"+str(pos_total_lien_mort_rangee)
        fontObj = Font(size=24, italic=True)
        self.worksheet[val_total_libelle].font = fontObj
        self.worksheet.row_dimensions[pos_total_lien_mort_rangee].height = 40

        self.worksheet[val_total_libelle].value = "TOTAL de lien\n mort total:"
        self.worksheet[val_total_libelle].alignment = Alignment(vertical="top",wrapText=True)
        self.worksheet.row_dimensions[pos_total_lien_mort_rangee].height = 80
        k+=1
        lien_mort_total=0
        for valeur_courante in  (global_var.liste_nombre_erreur):
            lien_mort_total += valeur_courante
        #var_dump(liste_nombre_erreur)
        val_total_libelle = "A"+str(global_var.espacement_tableau_stat+k)
        self.worksheet[val_total_libelle].value = str(lien_mort_total)+" liens morts "

        ####################################################################
        k+=global_var.espacement_tableau_stat
        pos_total_sitemap  = global_var.espacement_tableau_stat+k
        val_total_libelle = "A"+str(pos_total_sitemap)
        fontObj = Font(size=24, italic=True)
        self.worksheet[val_total_libelle].font = fontObj
        self.worksheet[val_total_libelle].value = "TOTAL sitemap+\nrobot.txt(confondu) :"
        self.worksheet.row_dimensions[pos_total_sitemap].height = 55

        k+=1
        val_total_libelle = "A"+str(global_var.espacement_tableau_stat+k)
        #global compteur_erreur_sitemap
        self.worksheet[val_total_libelle].value = str(global_var.compteur_erreur_sitemap)+""


        ####################################################################

        k+=global_var.espacement_tableau_stat
        pos_liste_complete_site =global_var.espacement_tableau_stat+k
        val_total_libelle = "A"+str(pos_liste_complete_site)
        fontObj = Font(size=24, italic=True)
        self.worksheet.row_dimensions[pos_liste_complete_site].height = 40

        self.worksheet[val_total_libelle].font = fontObj
        self.worksheet[val_total_libelle].value = "LISTE COMPLETE DES SITES :"
        k+=1
        for site, position_courante in (global_var.dictionnaire_toute_position.iteritems()):
            val_total_libelle = "A"+str((global_var.espacement_tableau_stat+k))
            val_total_libelle2 = "B"+str((global_var.espacement_tableau_stat+k))
            self.worksheet[val_total_libelle].value = ''+site
            self.worksheet[val_total_libelle2].value = '=HYPERLINK("resultat_affichage!$'+position_courante+'","LIEN")'
            self.worksheet[val_total_libelle2].style = 'Hyperlink'
           # self.worksheet[val_total_libelle2].hyperlink = ("http://"+position_courante.value+"")
            k+=1

        ####################################################################

        k+=global_var.espacement_tableau_stat
        pos_liste_complete_site =global_var.espacement_tableau_stat+k
        val_total_libelle = "A"+str(pos_liste_complete_site)
        fontObj = Font(size=24, italic=True)
        self.worksheet.row_dimensions[pos_liste_complete_site].height = 60
        self.worksheet[val_total_libelle].font = fontObj
        self.worksheet[val_total_libelle].value = "LISTE COMPLETE DES \n SITES AVEC ERREURS:"
        k+=1

        for probleme, sites in (global_var.liste_global_site_erreur.iteritems()):
            val_total_libelle2 = "B"+str((global_var.espacement_tableau_stat+k))

            val_total_libelle = "A"+str((global_var.espacement_tableau_stat+k))
            self.worksheet[val_total_libelle].value = ''+sites
            bleu_fond = PatternFill(start_color='0000ff', end_color='0000ff', fill_type='solid')
            self.worksheet[val_total_libelle].fill = bleu_fond
            #self.worksheet[val_total_libelle].value = probleme
            self.worksheet[val_total_libelle2].value = probleme
            k+=1
