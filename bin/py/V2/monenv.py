#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

#from __future__ import absolute_import

import pprint
import subprocess
import os
from logger import Logger

def initialiser_env(base):
    os.chdir(base)
    chemin_actuel = os.getcwd()
    #call(["source", ""])
    #call(["source", "bin/config/chemin_fichier.sh"])


    command  = ['bash', '-c', 'source bin/config/chemin.sh && env']
    command2 = ['bash', '-c', 'source bin/config/chemin_fichier.sh && env']
    
    proc  = subprocess.Popen(command, stdout = subprocess.PIPE)
    proc2 = subprocess.Popen(command2, stdout = subprocess.PIPE)


    for line in proc.stdout:
      (key, _, value) = line.partition("=")
      os.environ[key] = value

    proc.communicate()


    for line in proc2.stdout:
      (key, _, value) = line.partition("=")
      os.environ[key] = value

    proc2.communicate()

    global GLOBAL_TMP
    global GLOBAL_BASE_SOURCE 
    global GLOBAL_BASE_BIN
    global GLOBAL_BASE_BIN_PY
    global GLOBAL_BASE_VENDOR
    global GLOBAL_BASE_SOURCE
    global GLOBAL_BASE_MES_LOG 
    global GLOBAL_BASE_MES_LOG 
    global GLOBAL_CHEMIN_CSV
    global GLOBAL_CHEMIN_XLSX
    global GLOBAL_BASE_SORTIE
    global GLOBAL_CHEMIN_XLS
    
    GLOBAL_TMP          = os.environ["TMP"].replace("\n","")
    GLOBAL_BASE_SOURCE  = os.environ["BASE_SOURCE"].replace("\n","")
    GLOBAL_BASE_BIN     = os.environ["BASE_BIN"].replace("\n","")
    GLOBAL_BASE_BIN_PY  = os.environ["BASE_BIN_PY"].replace("\n","")
    GLOBAL_BASE_VENDOR  = os.environ["BASE_VENDOR"].replace("\n","")
    GLOBAL_BASE_SOURCE  = os.environ["BASE_SOURCE"].replace("\n","")
    GLOBAL_BASE_MES_LOG = os.environ["BASE_MES_LOG"].replace("\n","")
    
    GLOBAL_CHEMIN_CSV   = os.environ["CHEMIN_CSV"].replace("\n","")
    GLOBAL_CHEMIN_XLSX  = os.environ["CHEMIN_XLSX"].replace("\n","")
    #longueur_xls        = len(GLOBAL_CHEMIN_XLSX)
    GLOBAL_CHEMIN_XLS   = os.environ["CHEMIN_XLS"].replace("\n","")
    
    GLOBAL_BASE_SORTIE  = os.environ["BASE_SORTIE"].replace("\n","")
    

    global LOGGER 
    LOGGER = Logger()

    os.chdir(chemin_actuel)
    # print("change le env")
    # print(GLOBAL_CHEMIN_CSV)