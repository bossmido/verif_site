#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from pprint     import pprint
import types

import sys 
import  os
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:

    from Cellule    import Cellule
    from Test       import Test
    from Position   import Position
    from fonction   import *
    
else:
    from V2.Cellule    import Cellule
    from V2.Test       import Test
    from V2.Position   import Position
    from V2.fonction   import *



class CelluleContenu(Cellule):
    """Class CelluleContenu
    """
    # Attributes:
    
    liste_correspondance    = {}  # (dict) 
    vide                    = {}
    fichier_manquant   = False
    # Operations
    def __init__(self,feuille_ou_ecrire="",position_base=Position(),position_relative=Position(),liste_correspondance=None,transformation=None,couleur=None,fond=None,border=None,fichier_manquant=False):
        super(CelluleContenu,self).__init__(feuille_ou_ecrire=feuille_ou_ecrire,position_base=position_base,position_relative=position_relative,transformation=transformation,couleur=couleur,fond=fond,border=border)

        self.fichier_manquant=fichier_manquant
        if  liste_correspondance != None:
            self.liste_correspondance=liste_correspondance
        self.vide={'vide':'vide'}
        self.liste_correspondance={
        'erreur_404':{"-1":"indisponible","-2":"peut-etre indisponible","N":"nombre de page 404"},
        'sitemap':{"OK":"sitemap trouve","KO":"sitemap non trouve"},
        'robot':{"OK":"robot.txt trouve","KO":"robot.txt non trouve"},
        'title':{"OK":"balise meta title avec description  trouve","KO":"balise meta title avec description non trouve"},
        'semantique' :  self.vide,#pas utilise il me semble
                           #
        'semantique_complexe' : self.vide,
        'rien' : self.vide,
        'vide' : self.vide

        }    
        self.couleur    = None
        self.fond       = "c0c0c0"   
        self.border     = True


    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        super(CelluleContenu, self).dessiner(position=position,taille=taille,transformation=transformation,couleur=couleur,fond=fond,border=border,indice=indice)


    def dessiner_cellule(self,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,type_cellule=Test.rien):
        super(CelluleContenu, self).dessiner_cellule(transformation=transformation,couleur=couleur,fond=fond,border=border,indice=indice,type_cellule=type_cellule)

        if(type_cellule == None):
            raise("manque parametre de type de cellule")

        for cle , val in self.liste_correspondance[type_cellule.__str__()].items() :
            if RepresentsInt(cle):
                if self.get_val() == cle:
                    self.set_val(val)
                if "N" == cle:
                    return
            else:
                print(self.objet_cell.value)

                tab_parse=self.objet_cell.value.split(" ")
                if self.fichier_manquant:
                    if len(tab_parse)>3:
                        print("@@@@@@@@@@@ debug cell @@@@@@@@@@@@")
                        dic_info = eval(tab_parse[3:(len(tab_parse)-1)].__repr__())
                        print("")
                        print("")
                        pprint(dic_info)
                        print("")
                        print("")
                        #pprint(dic_info[0])
                        print(type(dic_info).__name__)
                        print("longueur liste : "+str(len(tab_parse[3:(len(tab_parse)-1)]) ) )

                        print("")
                        print("")
                        ##exit()
                        if  len(tab_parse[3:(len(tab_parse)-1)] ) == 0:
                            pass
                            self.set_val("fichiers manquants : nombre " + tab_parse[2] +": \n\n\n")   
                        else:
                            pass
                            self.set_val("fichiers manquants : nombre " + tab_parse[2] +" : \n\n\n"+tab_parse[3:(len(tab_parse)-1)].__repr__())   

                    if len(tab_parse)==4 and tab_parse[0] == 0:
                        self.set_val("aucun probleme")

                    if len(tab_parse)>0 and tab_parse[0] == "-2":
                        self.set_val("peut etre injoignable")

                if self.get_val() == cle:
                    self.set_val(val)                    
        self.dessiner(feuille_ou_ecrire=self.feuille_ou_ecrire,taille=taille,transformation=transformation,couleur=couleur,fond=fond,border=border,indice=indice)