#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import  pprint
# from Couleur import Couleur
from    enum                    import Enum
from    openpyxl                import *
from    openpyxl.drawing.fill   import PatternFillProperties, ColorChoice
from    openpyxl.styles         import PatternFill, Border, Side, Alignment, Protection, Font, Color

import sys
from    os.path     import dirname, basename, isfile
if   sys.version_info.major   == 2:    
    from    Dessinable              import Dessinable
else:
    from    V2.Dessinable              import Dessinable


class Couleur(Enum):
    """Class Couleur
    """
    # Attributes:
    noir            ="000000"
    defaut          ="000000"
    couleur_header  ="002b43"
    rouge           ="c90000"
    orange          ="ffa500"
    jaune           ="ffff00"
    bleu            ="0057c9"
    gris            ="d3d3d3"
    blanc           ="ffffff"

    # def __init__(self):
    #     pass
