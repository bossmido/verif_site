#!/usr/bin/env python2.7Position

# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import absolute_import

from future.utils import bytes_to_native_str as n
#__package__ =n(b"V2")

import sys 
import pprint

import  os
from    os.path     import dirname, basename, isfile
import subprocess

if   sys.version_info.major  == 2:

    import  global_var
    from    monenv         import *  
    from    XLS            import * 
    from    Position       import *
else:
    from V2.global_var     import *
    from V2.monenv         import *
    from V2.XLS            import *
    from V2.Position       import *


def __init__():
    __all__ = ["pprint","fonction"]
    # modules = glob.glob(dirname(__file__)+"/*.py")
    # __all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]

    base_test="/home/thi/bin/verif_site"
    initialiser_env(os.getcwd())

    print("CREATION DU XLS")
    liste_feuille_de_CSV={"csv":"resultat_csv","site":"resultat_affichage","stat":"resultat_statistique"}
    xls_instancie = XLS(liste_feuille_de_CSV)
    xls_instancie.dessiner()
    xls_instancie.enregistre()
    

    longueur_chaine_sortie      = len(monenv.GLOBAL_BASE_SORTIE)
    os.chdir(monenv.GLOBAL_BASE_SORTIE)
    if not os.path.isdir(".git"): 
        print("CREATION DU REPO GIT")
        p = subprocess.call('git init',shell=True)
        q = subprocess.call('git add .',shell=True)
    commentaire=""
    pprint.pprint(sys.argv)
    if len(sys.argv)>1:
        commentaire = str(sys.argv[1])
    else:
        commentaire="commentaire vide"
    #commentaire="test"
    print("COMMIT DU DEPOT")
    #r = os.popen('git -am "'+commentaire+'" ',"r")
    print(n('git commit -am "'+commentaire+'"'))
    args = ['git','commit','-am','"'+commentaire+'"']
    r  = subprocess.call(args,shell=True,stdout=subprocess.PIPE)
    print("FIN")
  

if __name__ == '__main__':
    __init__()

#__package__="V2"
# if __package__:
#     from Position import Position
#     