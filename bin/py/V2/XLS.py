#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from    openpyxl            import *
import  pprint

import sys 

from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    from    TableauSiteListe   import TableauSiteListe
    from    TableauStat        import TableauStat
    from    TableauGenerique   import TableauGenerique
    from    TableauCSV         import TableauCSV

    from    Couleur            import Couleur
    from    Dessinable         import Dessinable
    from    Couleur            import Couleur
    import  global_var 
    import  monenv 


else:
    from    V2.TableauSiteListe   import TableauSiteListe
    from    V2.TableauStat        import TableauStat
    from    V2.TableauGenerique   import TableauGenerique
    from    V2.TableauCSV         import TableauCSV

    from    V2.Couleur            import Couleur
    from    V2.Dessinable         import Dessinable
    from    V2.Couleur            import Couleur
    import  V2.global_var 
    import  V2.monenv 




class XLS(Dessinable):
    """Class xls
    """
    # Attributes:
    workbook=None
    couleur=None
    liste_global_site_erreur=None
    liste_feuille=None
    ##
    tableau_csv=None
    tableau_stat=None
    tableau_site=None
    filename_xlsx_c=None
    def __init__(self,liste_feuille_string):
        super(XLS,self).__init__()
        global_var.WORKBOOK_GLOBAL = Workbook()
        ##
        self.workbook = global_var.WORKBOOK_GLOBAL
        self.couleur = Couleur(Couleur.defaut)
        self.liste_global_site_erreur = {'erreur_404':' ','sitemap':' ','robot':' ','title':' ','semantique' : ' ','semantique_complexe' : ' '}
        self.liste_feuille = ()
     
        # for key,vaGLOBAL_CHEMIN_XLSl in liste_feuille_string :
        #     self.liste_feuille[val] =  global_var.WORKBOOK_GLOBAL.create_sheet(val)

        self.tableau_site_liste = TableauSiteListe(liste_feuille_string["site"])
        self.tableau_stat       = TableauStat("A",1,liste_feuille_string['stat'])
        self.tableau_csv        = TableauCSV("A",1,liste_feuille_string['csv'])

        ##
        #couleurs = Couleur
        # Operations
        
    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        
        longueur        = len(monenv.GLOBAL_CHEMIN_XLSX)
        longueur_2      = len(monenv.GLOBAL_BASE_SORTIE)

        filename_xlsx_c =  monenv.GLOBAL_CHEMIN_XLSX
        self.filename_xlsx_c = filename_xlsx_c
        filename_xls_c  =  monenv.GLOBAL_CHEMIN_XLS
        filename_csv_c  =  monenv.GLOBAL_CHEMIN_CSV[0:(longueur-1)]
        
        pprint.pprint("@@@@@@@@@@@@@@@@@@")

        pprint.pprint(filename_csv_c)
        pprint.pprint(filename_csv_c)
        pprint.pprint(filename_csv_c)
        pprint.pprint(filename_csv_c)
        pprint.pprint(filename_csv_c)

        if not os.path.exists(filename_xlsx_c):
            os.mknod(filename_xlsx_c)
        if not os.path.exists(filename_xls_c):
            os.mknod(filename_xls_c)
        #if not os.path.exists(filename_csv_c):
            #os.mknod(filename_csv_c)
        
        filename_xlsx   = open(filename_xlsx_c,"rw")
        fichier_xls     = open(filename_xls_c,"rw")
        fichier_csv     = open(filename_csv_c,"rw")

        longueur        = len(monenv.GLOBAL_CHEMIN_XLSX)
        longueur_2      = len(monenv.GLOBAL_BASE_SORTIE)

        #self.workbook = load_workbook(monenv.GLOBAL_BASE_SORTIE[0:(longueur_2-1)]+"/"+monenv.GLOBAL_CHEMIN_XLSX[1:(longueur-1)])


        ##
        self.tableau_csv.dessiner() 
        self.tableau_site_liste.dessiner() 
        self.tableau_stat.dessiner() 
        ##
        try:
            std = self.workbook.get_sheet_by_name('Sheet')
            self.workbook.remove_sheet(std)
        except:
                return
    def enregistre(self):
        longueur        = len(monenv.GLOBAL_CHEMIN_XLSX)
        longueur_2      = len(monenv.GLOBAL_BASE_SORTIE)

        self.workbook.save(self.filename_xlsx_c)