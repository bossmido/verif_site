#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys
from    os.path     import dirname, basename, isfile

if   sys.version_info.major   == 2:    
    from Position import Position
else:
    from V2.Position import Position



class Dessinable(object):
    #__metaclass__=dessinable
    """Abstract class Dessinable
    """
    # Attributes:
    
    # Operations


    # def __init__(self):
    #     pass

    def dessiner(feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        """function Dessiner
        
        returns 
        """
        raise NotImplementedError()
