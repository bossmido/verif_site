#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import pprint
#from pprint_data import data
import logging
from CouleurTerminal import CouleurTerminal

class Logger:
    
    """Class logger
    """ 

    sortie  = None
    col     = None

    def __init__(self):
        #sortie  = pprint(self, output=False)
        self.col     = CouleurTerminal()
    
    def log(self,msg):
        print(self.col.red+"msg"+self.col.reset)
    
    def pp_closure(self):
        return 