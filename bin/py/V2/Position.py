#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from math       import *
import copy
import inspect
import types
import sys

from    os.path     import dirname, basename, isfile

if   sys.version_info.major   == 2:    
    from Direction  import Direction
else:
    
    from V2.Direction  import Direction




class Position:
    """Class Position
    """
    # Attributes:
    x = None  # (String) 
    y = None  # (int) 
    __name__="Position"
    # Operations
    def __init__(self,x="A",y=1):
        # try:
        #     if(callable(getattr(x, isalpha, None )) and not x.isalpha()):
        #         Exception("Erreur du passage de coordonnee.")
        # except:
        #     print(x)
        #     print(x)
        #     print("\n\n")
        #     raise Exception("Erreur du passage de coordonnee")

        self.x = x 
        self.y = y 

    def copie(self):
        return Position(self.x,self.y)

    def get_x(self):
        return self.x.decode("latin-1")
    def get_y(self):
        return self.y

    def is_object(self,obj):
        return inspect.isclass(type(obj)) and not type(obj) == type

    def calculer_addition(self,lettre1,lettre2):
        # print("lettre 1")
        # print(lettre1)
        # print("lettre 2")
        # print(lettre2)
        #difference = ord(lettre1)-ord(lettre2)
        print("@@@@@@@@@@ calculer addition @@@@@@@@@@@@")
        print(ord(lettre1)-ord("A"))
        val_base_normaliser = ord(lettre1)-ord("A")
        val_base_normaliser2 = ord(lettre2)-ord("A")

        resultat = (val_base_normaliser2 + val_base_normaliser)%27
        # print("fin de decalage de caractere")
        # print( resultat+ord("A"))
        return chr(resultat+ord("A"))

    def calculer_soustraction(self,lettre1,lettre2):
        # print("lettre 1")
        # print(lettre1)
        # print("lettre 2")
        # print(lettre2)
        #difference = ord(lettre1)-ord(lettre2)
        val_base_normaliser = ord(lettre1)-ord("A")
        val_base_normaliser2 = ord(lettre2)-ord("A")

        resultat = abs(val_base_normaliser2 - val_base_normaliser)%27
        # print("fin de decalage de caractere")
        # print( resultat+ord("A"))
        return chr(resultat+ord("A"))

    def decomposer_coordonnee():
        
        retour = (self.x,self.y)
        return str(retour)

    # def get():
    #     retour = self.x+str(self.y)
    #     return retour

    def to_str(self,param):
        return (param.x+str(param.y)).decode("utf-8").encode("utf-8")


    def __str__(self):
        passage_param = self
        return self.to_str(passage_param)


    def __repr__(self):
        return "objet perso position ("+self.to_str(self)+")".decode("utf-8").encode("utf-8")
    
    def __add__(self, other):
        if(other == None):
            return self
        # print(other.__class__.__name__)
        if(isinstance(other,int)):
            return Position(self.x,self.y+other)

        if(isinstance(other,str) or isinstance(other,unicode)):
            if(other == ""):
                return self
            #print("debug :  lettre position")
            #print(self.calculer_addition(self.x,other))
            return Position(self.calculer_addition(self.x,other),self.y)
        if(isinstance(other,Position) or isinstance(other,unicode)):
            #print("debug :  lettre position")
            #print(self.calculer_addition(self.x,other.x))
            return Position(self.calculer_addition(self.x,other.x),self.y+other.y-1)
  
    def __sub__(self, other):
        if(isinstance(other,int)):
            return Position(self.x,self.y-other)
        if(isinstance(other,str) or isinstance(other,unicode)):
            if(other == ""):
                return self
            #print(self.calculer_soustraction(self.x,other))
            return Position(self.calculer_soustraction(self.x,other),self.y)
        if(self.is_object(Position)):
            #print(self.calculer_addition(self.x,other.x))
            return Position(self.calculer_addition(self.x,other.x),self.y-other.y)
        
    def __lt__(self, other):
        if(ord(self.x)<ord(other.x)):
            if(self.y<other.y):
                return True
        return False
    def ___le__(self, other):
        if(ord(self.x)<=ord(other.x)):
            if(self.y<=other.y):
                return True
        return False

    def __eq__(self, other):
        if(other == None):
            return False
        if(ord(self.x)==ord(other.x)):
            if(self.y==other.y):
                return True
        return False

    def __ne__(self, other):
        if(ord(self.x) != ord(other.x)):
            if(self.y != other.y):
                return True
        return False

    def __gt__(self, other):
        if(ord(self.x)<ord(other.x)):
            if(self.y<other.y):
                return True
        return False

    def __ge__(self, other):
        if(ord(self.x)<=ord(other.x)):
            if(self.y<=other.y):
                return True
        return False

    def decaler_coordonnee(chaine,nombre,decalage,direction=Direction.haut_bas):
        val=""
        if(Direction.haut_bas == direction):
            val = chr(ord(chaine)+decalage)+str(nombre)
        if(Direction.gauche_droite == direction):
            val = chaine+str(nombre + decalage)

        return val