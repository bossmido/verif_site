#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from enum import Enum

class Direction(Enum):
    """Class Direction
    """
    # Attributes:

    haut_bas        = 0  # (int) 
    gauche_droite   = 1  # (int) 
    
    # Operations
