#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from enum       import Enum
import sys

if   sys.version_info.major  == 2:
    from TestStr    import TestStr

else:
    from V2.TestStr    import TestStr


class Test(Enum):
    """Class Test
    """

    # Attributes:
    erreur_404          =  TestStr(1)
    sitemap             =  TestStr(2)
    robot               =  TestStr(3)
    title               =  TestStr(4)
    semantique          =  TestStr(5)
    rien                =  TestStr(6)
    semantique_complexe =  TestStr(7)
    
    # Operations
    def to_str(self):
        for cle,courant in self.liste_str.items():
            if self.val==int(cle):
                return courant
    
    def to_str(self):
        if self.value == None :
            return "vide"
        return 'my custom str! {0}'.format(self.value)

    def __repr__(self):
        return self.to_str()

    def __str__(self):
        return self.to_str()

