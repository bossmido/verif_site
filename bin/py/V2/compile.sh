#!/usr/bin/env bash

cython -2 --embed ./Cellule.py
cython -2 --embed ./CelluleContenu.py
cython -2 --embed ./CelluleLibelle.py
cython -2 --embed ./Cellule.py
cython -2 --embed ./Couleur.py
cython -2 --embed ./CouleurTerminal.py
cython -2 --embed ./Dessinable.py
cython -2 --embed ./Direction.py
cython -2 --embed ./fonction.py
cython -2 --embed ./global_var.py
cython -2 --embed ./__init__.py
cython -2 --embed ./logger.py
cython -2 --embed ./__main__.py
cython -2 --embed ./monenv.py
cython -2 --embed ./MonStyle.py
cython -2 --embed ./Position.py
cython -2 --embed ./RefCSV.py
cython -2 --embed ./StyleFond.py
cython -2 --embed ./StyleTexte.py
cython -2 --embed ./TableauCSV.py
cython -2 --embed ./TableauGenerique.py
cython -2 --embed ./TableauSiteListe.py
cython -2 --embed ./TableauSite.py
cython -2 --embed ./TableauStat.py
cython -2 --embed ./testPosition.py
cython -2 --embed ./Test.py
cython -2 --embed ./TestStr.py
cython -2 --embed ./XLS.py


# gcc -Os --shared -lpython2.7 -I /usr/include/python2.7  -fPIC -o  main  ./Cellule.c \
# ./CelluleContenu.c \
# ./CelluleLibelle.c \
# ./Cellule.c \
# ./Couleur.c \
# ./CouleurTerminal.c \
# ./Dessinable.c \
# ./Direction.c \
# ./fonction.c \
# ./global_var.c \
# ./__init__.c \
# ./logger.c \
# ./__main__.c \
# ./monenv.c \
# ./MonStyle.c \
# ./Position.c \
# ./RefCSV.c \
# ./StyleFond.c \
# ./StyleTexte.c \
# ./TableauCSV.c \
# ./TableauGenerique.c \
# ./TableauSiteListe.c \
# ./TableauSite.c \
# ./TableauStat.c \
# ./testPosition.c \
# ./Test.c \
# ./TestStr.c \
# ./XLS.c;


gcc -Os --shared -lpython2.7 -I /usr/include/python2.7  -fPIC -o  main  ./Cellule.c ./CelluleContenu.c ./CelluleLibelle.c ./Cellule.c  ./Couleur.c ./CouleurTerminal.c ./Dessinable.c ./Direction.c ./fonction.c ./global_var.c ./__init__.c  ./logger.c  ./__main__.c  ./monenv.c  ./MonStyle.c ./Position.c ./RefCSV.c ./StyleFond.c ./StyleTexte.c ./TableauCSV.c  ./TableauGenerique.c ./TableauSiteListe.c ./TableauSite.c ./TableauStat.c ./testPosition.c ./Test.c ./TestStr.c ./XLS.c;