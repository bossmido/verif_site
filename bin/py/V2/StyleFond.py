#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys 
import  os
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    from MonStyle import MonStyle
else:
    from V2.MonStyle import MonSty
class StyleFond(MonStyle):
    """Class StyleFond
    """
    # Attributes:
    
    # Operations

