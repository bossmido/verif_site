#!/usr/bin/env python2.7


# -*- coding: utf-8 -*-

from __future__ import unicode_literals



import sys 
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:

    import  global_var
    import  monenv
    from    RefCSV              import RefCSV
    from    TableauGenerique    import TableauGenerique
    from    Position            import Position
    from    Direction           import Direction
    from    Test                import Test
    from    Couleur             import Couleur
    from    CelluleLibelle      import CelluleLibelle
    from    CelluleContenu      import CelluleContenu

else:
    import  V2.global_var
    import  V2.monenv

    from    V2.RefCSV              import RefCSV
    from    V2.TableauGenerique    import TableauGenerique
    from    V2.Position            import Position
    from    V2.Direction           import Direction
    from    V2.Test                import Test
    from    V2.Couleur             import Couleur
    from    V2.CelluleLibelle      import CelluleLibelle
    from    V2.CelluleContenu      import CelluleContenu




from    openpyxl.styles     import PatternFill, Border, Side, Alignment, Protection, Font, Color

from    pprint              import pprint
# from pprint import pprint as pp

#from pprint_data import data

def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def affecter_val(cell,destination,site,test=Test.rien):
    global compteur_erreur_sitemap
    global liste_global_site_erreur

    std     = global_var.WORKBOOK_GLOBAL.get_sheet_by_name('resultat_csv')
    std1    = global_var.WORKBOOK_GLOBAL.get_sheet_by_name('resultat_affichage')


    cell    = std[cell]
    cell2   = std1[destination]

    bleu        = PatternFill(start_color='0057c9', end_color='0057c9', fill_type='solid')
    rouge_ref   = rouge = PatternFill(start_color='c90000', end_color='c90000', fill_type='solid')
    rouge_leger_ref  = PatternFill(start_color='ff4d4d', end_color='ff4d4d', fill_type='solid')

    orange      = PatternFill(start_color='ffa500', end_color='ffa500', fill_type='solid')
    vert        = PatternFill(start_color='00ff00', end_color='00ff00', fill_type='solid')
    gris        = PatternFill(start_color='d3d3d3', end_color='d3d3d3', fill_type='solid')

    if(cell.value != None ):
        tab_parse=cell.value.split(" ")

        if((test == Test.semantique_complexe) and (cell.value != None) and (len(cell.value)>10 ) ):
            global_var.liste_global_site_erreur['semantique_complexe'] += " "+site
        if(len(tab_parse)==1 and  test !=  Test.erreur_404 ):
            if(len(tab_parse)==1 and not RepresentsInt(tab_parse[0])):
                if(cell.value == "KO" ):
                    cell.fill   = rouge_ref
                    cell2.fill  = rouge_ref
                    if(test==Test.sitemap):
                        global_var.liste_global_site_erreur['sitemap'] += " "+site
                        global_var.compteur_erreur_sitemap+=1
                    if(test == Test.erreur_404):
                        global_var.liste_global_site_erreur['erreur_404'] += " "+site
                    if(test == Test.robot):
                        global_var.liste_global_site_erreur['robot'] += " "+site
                    if(test == Test.title):
                        global_var.liste_global_site_erreur['title'] += " "+site
                else:
                    cell.fill   = bleu
                    cell2.fill  = bleu
        else:
            if RepresentsInt(cell.value):
                if( test == Test.erreur_404 and int(cell.value )==(-2)  ):
                    if(test == Test.erreur_404):
                        global_var.liste_position_erreur.append(destination)

                        global_var.liste_global_site_erreur['erreur_404'] += " "+site
                        cell.fill   = rouge_leger_ref
                        cell2.fill  = rouge_leger_ref
                        return
            if RepresentsInt(cell.value):
                if( test == Test.erreur_404 and  int(cell.value )==(-1)):
                    cell.fill = cell2.fill = rouge
                    if(test == Test.erreur_404):
                        global_var.liste_position_erreur.append(destination)
                        global_var.liste_global_site_erreur['erreur_404'] += " "+site

                        cell.fill   = rouge_ref
                        cell2.fill  = rouge_ref
                        return

            if(len(tab_parse)>=3 and test == Test.erreur_404 ):

                tab_parse[1]  = "|"
                concatenation = ""
                if len(tab_parse) > 2 :
                    if len(tab_parse) > 3 :
                        print("@@@@@@@ debuggage 404 @@@@@@@@@@@@")
                        print("longueur fichier manquant "+ str(len(tab_parse[3:len(tab_parse)])))
                        #pprint.pprint( tab_parse[3:len(tab_parse)].__str__().decode(encoding='UTF-8',errors='ignore').encode("ascii"))
                        if tab_parse[3:len(tab_parse)].__str__() == "[u'[]']" :
                       
                            concatenation =  tab_parse[1]+" " + tab_parse[2]
                            concatenation = concatenation
                            cell.fill   = vert
                            cell2.fill  = vert

                        else:

                            concatenation =  tab_parse[1]+" " + tab_parse[2] +" "
                            concatenation = concatenation +  tab_parse[3:len(tab_parse)].__str__().decode(encoding='UTF-8',errors='ignore').encode("ascii")
                        
                else:
                    concatenation =  tab_parse[1]
                cell.value = tab_parse[0] + concatenation
                if RepresentsInt(tab_parse[0]):
                    if(int(tab_parse[0])==(-1)):
                        cell2.fill  = rouge
                        cell.fill   = rouge
                        if(test == Test.erreur_404):
                            #global_var.liste_position_erreur.append(destination)
                            global_var.liste_global_site_erreur['erreur_404'] += " "+site

                if RepresentsInt(tab_parse[2]):
                    if(int(tab_parse[2])>=1):
                        if(test == Test.erreur_404):
                            global_var.liste_global_site_erreur['erreur_404'] += " "+site
                            global_var.liste_position_erreur.append(destination)
                            global_var.liste_nombre_erreur.append(int(tab_parse[2]))
                            cell.fill  = orange
                            cell2.fill = orange
                    if(int(tab_parse[2])==0):
                        #cell.value  = "0"
                        cell.fill   = vert
                        cell2.fill =  vert

                if RepresentsInt(tab_parse[0]) and RepresentsInt(tab_parse[2]):
                    if(int(tab_parse[0])==0 and int(tab_parse[2])==-1):
                        cell.value  = "site completement innaccesible"
                        cell.fill   = rouge_ref
                        cell2.fill  = rouge_ref

    else:
        cell.value = "VIDE";


class TableauSite(TableauGenerique):
    """Class TableauSite
    """
    # Attributes:
    liste_entree_tableau_site = ()
    liste_entree_tableau_site_correspondance = ()
    site_courant=None
    indice=0
    # Operations
    def __init__(self,feuille="",x="A",y=1,liste_taille=(),nombre_ligne=0,type_entree=Direction.gauche_droite,indice=0):
        liste_taille=(90,25,25,25,25,100,25)
        liste_taille_colonne=(35,15,35)

        super(TableauSite,self).__init__(x=(x),y=(y+7),liste_taille=liste_taille,nombre_ligne=nombre_ligne,type_entree=type_entree,feuille_nom=feuille)
        global liste_entree_tableau_site

        self.indice = indice
        self.liste_entree_tableau_site = global_var.liste_entree_tableau_site
        self.liste_entree_tableau_site_correspondance = global_var.liste_entree_tableau_site_correspondance

        self.site_courant = RefCSV.charger(monenv.GLOBAL_CHEMIN_CSV)[indice]
        self.changer_taille_ligne(liste_taille)
        self.changer_taille_colonne(liste_taille_colonne)
        self.couleur_fond(liste_taille,liste_taille_colonne)


    def __repr__(self):
        super(TableauSite, self).__repr__()
        print("information du SiteTableau")
        if self.indice == None or self.indice == 0 :
            print("afichage vide")
        else:
            print(""+str(self.indice))

    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,indice=0,border=False,valeur_a_ajouter=""):

        position_debut = position

        ws_tmp = global_var.WORKBOOK_GLOBAL.get_sheet_by_name(self.feuille_nom)
        ws_csv = global_var.WORKBOOK_GLOBAL.get_sheet_by_name("resultat_csv")
        ws_tmp["A1"].value="verification de la SEO"
        fontObj = Font(size=24, italic=True)
        ws_tmp['A1'].font = fontObj
        position_csv = Position("A",indice+1)
        ws_tmp.row_dimensions[1].height = 45

        #print( RefCSV.charger(monenv.GLOBAL_CHEMIN_CSV))
        inc=0
        for val in self.site_courant:
            nom_site=self.site_courant[0]

            # print(self.feuille_nom)
            # print((position+inc)+"")
            # print(position+inc)
            # print(position+inc)
            # print(inc)

            #################################################
            #                 1 libelle                     #
            #################################################
            position_xy = (position+inc+"")
            
            chaine_position = position_xy.__str__()

            cell_libelle_affichage = CelluleLibelle(self.feuille_nom,position_xy,None)

            if(len(self.liste_entree_tableau_site) > inc
             ):
                cell_libelle_affichage.set_val(self.liste_entree_tableau_site[inc]+"")
                cell_libelle_affichage.dessiner_cellule()

            #################################################
            #                 2 valeur                      #
            #################################################
            res_cellule=""

            chaine_position2 = ((position+"C")+(inc-1))
            chaine_titre=((chaine_position2)-"C")
            pass_inc=False
            if((len(self.liste_entree_tableau_site_correspondance))>inc ):
                if inc==0:#url
                    pass_inc=True
                    ###chaine_titre=((chaine_position2-1)-"C").__str__()
                    ###ws_tmp[chaine_titre].value = val
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_titre,None)
                    cell_contenu_affichage.set_val(val)
                    cell_contenu_affichage.set_font(18)
                    cell_contenu_affichage.set_couleur(Couleur.bleu)

                    cell_contenu_affichage_lien = CelluleContenu(self.feuille_nom,chaine_titre+"B",None)

                    if len(self.site_courant) == 8:
                        cell_contenu_affichage_lien.set_lien(self.site_courant[7],self.site_courant[7])
                    cell_contenu_affichage_lien.set_couleur(Couleur.blanc)

                    global_var.dictionnaire_toute_position.update({nom_site : val})
                else:
                    pass
                    # cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    # cell_contenu_affichage.set_val(val)
                    ###ws_tmp[chaine_position2.__str__()].value = val

                if inc==1:#404 not found
                    pass_inc=True#404 not found
                    pass
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None,fichier_manquant=True)
                    
                    cell_contenu_affichage.set_val(val.decode(encoding='UTF-8',errors='ignore').encode("ascii"))
                if inc==2:# sitemap
                    pass_inc=True
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    cell_contenu_affichage.set_val(val)
                    pass
                if inc==3:#robot.txt
                    pass_inc=True
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    cell_contenu_affichage.set_val(val)
                    pass
                if inc==4:#description
                    pass_inc=True
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    cell_contenu_affichage.set_val(val)
                    pass
                if inc==5:#semantique
                    pass_inc=True
                    chaine_position2 = ((position+"C")+(inc))
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    val  = val.replace("_", " ")
                    cell_contenu_affichage.set_val(val)
                    pass
                if inc==6:#commentaire
                    pass_inc=True
                    chaine_position2 = ((position+"C")+(inc-2))
                    cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    cell_contenu_affichage.set_val(val)
                    cell_contenu_affichage.set_alignement()
                    cell_contenu_affichage.set_couleur(Couleur.blanc)
                    pass
                    
               ###pas utilise
                if inc==7:
                    pass_inc=True
                    # chaine_position2=((chaine_position2-2)-"C")
                    # cell_contenu_affichage = CelluleContenu(self.feuille_nom,chaine_position2,None)
                    # cell_contenu_affichage.set_val(val)
                if inc==8:#site_libelle
                    pass_inc=True

                    pass
                #ws_tmp[position].value = val
            if pass_inc:
                cell_contenu_affichage.dessiner_cellule()


            if(len(self.liste_entree_tableau_site_correspondance)>inc ):
                pass
                if(chaine_position == None):
                    continue
                position_affectation_str = (((chaine_position2)+2)).__str__()

                if inc==5:
                    print("debut de position")
                    print(position_csv.__str__())
                    print(position_affectation_str.__str__())
                    print("fin de position")
                    #exit()

                affecter_val(position_csv.__str__(),position_affectation_str,nom_site,test = self.liste_entree_tableau_site_correspondance[inc])

            print(position_csv)
            position_csv=position_csv+"B"
            inc = inc + 1

        position_fin = ((position+"C")+(inc))


        chaine_rectangle_selection = (position_debut+1).__str__()+":"+(position_fin-1).__str__()
        self.set_border(chaine_rectangle_selection)

    def __str__(self):
        #self_function.get(self_function)
        return str(self.indice)+" : "+self.site_courant.__str__()
        #return retour

    def len(self):
        return super(TableauSite,self).len()