#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals




import sys 
import  os

from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    import  monenv
    import  global_var

    from  MonStyle           import MonStyle
    from  Cellule            import Cellule
    from  Dessinable         import Dessinable
    from  Position           import Position
    from  Direction          import Direction
    from  RefCSV             import RefCSV
    from  Couleur            import Couleur

else:
    import V2.monenv
    import V2.global_var
    from V2.MonStyle           import MonStyle
    from V2.Cellule            import Cellule
    from V2.Dessinable         import Dessinable
    from V2.Position           import Position
    from V2.Direction          import Direction
    from V2.RefCSV             import RefCSV
    from V2.Couleur            import Couleur



from openpyxl.styles    import PatternFill
from openpyxl.styles    import PatternFill, Border, Side, Alignment, Protection, Font, Color

import pprint
import copy
import re

def ensure_unicode(v):
    if isinstance(v, str):
        pass
        #v = v.decode('utf8')
    #return unicode(v)
    return v
def changer_taille(feuille,position_base,liste_taille,type_entree=Direction.gauche_droite):
    if position_base == None:
        return
    pprint.pprint(liste_taille)
    position_base_str = position_base.__str__()
    inc=0
    for taille in liste_taille:
        if(type_entree == Direction.gauche_droite):
            feuille.columns_dimensions[position_base.get_x()].width = taille
            position_base = position_base + 1
        if(type_entree == Direction.haut_bas):
            feuille.row_dimensions[position_base.y].height    = taille
            position_base = position_base + "B"
    


class TableauGenerique(Dessinable):
    """Class TableauGenerique
    """
    # Attributes:

    position_absolue    = None  # (Position) 
    direction           = None  # (int) 
    feuille_nom         = None  # (String) 
    worksheet           = None
    taille_tableau      = None
    #######################################
    liste_cellule       =[[]]
    style_tableau={}

    # Operations
    def __init__(self,x="A",y=1,feuille_nom="vide",direction=None,espace_colonne=None,espace_ligne=None,liste_taille=(),type_entree=Direction.gauche_droite,nombre_ligne=0,style_tableau={"couleur":"","fond":"","border":""}):
        super(TableauGenerique,self).__init__()

        self.position_absolue = Position(x,y) 
        self.direction=direction
        self.style_tableau = style_tableau
        feuille_existe_deja=False

        try:
            global_var.WORKBOOK_GLOBAL.get_sheet_by_name(feuille_nom)
        except:
            feuille_existe_deja=True
            pass

        if(feuille_nom !="vide"):
            self.feuille_nom = feuille_nom
        if(feuille_nom !="vide" and ( feuille_existe_deja) ):
            global_var.WORKBOOK_GLOBAL.create_sheet(self.feuille_nom)
        self.worksheet = global_var.WORKBOOK_GLOBAL.get_sheet_by_name(self.feuille_nom)
        
        self.worksheet.sheet_properties.pageSetUpPr.fitToPage = True
        self.worksheet.page_setup.fitToHeight = False
        
        self.taille_tableau = nombre_ligne
        print("test taille : ")
        print(self.position_absolue)

             
    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        pp = pprint.PrettyPrinter(indent=4)
        position_depart=Position("A",1)
        position_courant = copy.copy(position_depart)
        indice_courant=0
        inc_ref=-3
        pprint.pprint(monenv.GLOBAL_CHEMIN_CSV)

        for rangee in RefCSV.charger(monenv.GLOBAL_CHEMIN_CSV):
            taille  = 8 # nombre d entre fixe
            inc = 0
            for val in rangee:
                if inc == 5 : # semantique ecrit en separation de tiret bas
                    val = val.replace("_", " ")
                    rangee[inc] = val
                if inc == 6 : # commentaire
                    rangee[inc] = "=resultat_affichage!"+Position("C",inc_ref).__str__()
                # print("titre:")
                # print(val)
                # val = val.replace("\xa6","")
                # val = val.replace("\xc3","")
                # val = val.replace("\xa9","")
                # val = val.replace("\xa0","")
                # val = val.replace("\xaa","")
                # val = val.replace("\xe2","")
                # val = val.replace("\x80","")
                # val = val.replace("\xa6","")


                rangee[inc] = val.decode("utf-8",errors='ignore')
                #val = val.decode("utf-8",errors='ignore').encode("utf-8",errors='ignore')
                # val = val.replace('\\x', '').decode('string_escape')
                # val = re.sub(r'[^\x00-\x7f]',r'', val) 
                # val=val.decode("utf-8")
                inc = inc + 1

            self.worksheet.append(rangee)
            position_courant = position_courant + 1
            inc_ref = inc_ref + taille + global_var.espacement_tableau_site
            
            indice_courant = indice_courant + 1
        print("position fin")
        print(position_courant)
        print(position_courant)
        print((position_courant + "H"))
        
        position_fin = (position_courant + "H") - 1

        chaine_rectangle_selection = position_depart.__str__()+":"+position_fin.__str__()
        print("tableau csv")
        print(chaine_rectangle_selection)
        self.set_border(chaine_rectangle_selection)
        self.set_titre_rangee()
        liste_taillle_egale = []
        for l in range(0,9):
            liste_taillle_egale.append(22)
        pprint.pprint(l)
        self.changer_taille_colonne(liste_taillle_egale)

    def ajouter_cellule(self,cellule,indice):
        if(len(self.liste_cellule)<indice):
            self.liste_cellule.append([])
        self.liste_cellule[indice].append(cellule)

    def dessiner_cellule(self):
        for cel in self.liste_cellule:
            cel.dessiner_cellule()

    def  __getitem__(self,key):
        return self.liste_cellule[key]
        if(isinstance(key,Position)):
            return self.liste_cellule[key.__str__()]
        else:
            return self.liste_cellule[key] 

    def len(self):
        return self.taille_tableau
    def  __setitem__(self, key, item):
        if(isinstance(key,Position)):
            self.liste_cellule[key.__str__()] = item
        else:
            self.liste_cellule[key] = item

    def set_titre_rangee(self,rangee=Position("A",1),longueur=8,taille=24):

        rows = self.worksheet[Position("A",1).__str__()+":"+(Position("A",1)+chr(ord("A")+longueur)).__str__()]
        for cell in rows:
            for c in cell:

                fontObj = Font(size=taille, italic=None)
                c.font = fontObj
                    #cell.border = border

    def set_border(self,cell_range):
        border = Border(left=Side(border_style='thin', color='000000'),
                    right=Side(border_style='thin', color='000000'),
                    top=Side(border_style='thin', color='000000'),
                    bottom=Side(border_style='thin', color='000000'))

        rows = self.worksheet[cell_range]
        for row in rows:
            for cell in row:
                cell.border = border

    def __repr__(self):
        return ""
    def changer_taille_colonne(self,liste_taille,type_entree=Direction.gauche_droite):
        #position_de_base=self.position_absolue
        position_courante = self.position_absolue


        position_courante_str = position_courante.__str__()
        inc=0
        for taille in liste_taille:
            if position_courante == None:
                return
            print("position courante")
            pprint.pprint(position_courante.get_x())
            self.worksheet.column_dimensions[position_courante.get_x()].width = taille
            position_courante = position_courante + "B"
          
    def couleur_fond(self,liste_taille_longueur,liste_taille_largeur):
        position_courante = self.position_absolue.copie()

        kx=position_courante.get_x()
        ky=position_courante.y

        for y in liste_taille_largeur:
            if position_courante == None:
                return 
            pos = position_courante.copie()
            for x in liste_taille_longueur:

                #self.worksheet[position_courante].fill = PatternFill(start_color='FFEE1111', end_color='FFEE1111', fill_type='solid')
                self.worksheet[pos.__str__()].fill=PatternFill(fill_type="solid", start_color=Couleur.gris, end_color=Couleur.gris)
                pos = pos + 1
            position_courante = position_courante + "B"


    def changer_taille_ligne(self,liste_taille,type_entree=Direction.gauche_droite):
        #position_de_base=self.position_absolue
        position_courante = self.position_absolue
        print("debut")
        print(position_courante)
        if position_courante == None:
            return
        #pprint.pprint(liste_taille)
        position_courante_str = position_courante.__str__()
        inc=0

        for taille in liste_taille:
            self.worksheet.row_dimensions[position_courante.y].height = taille
            position_courante = position_courante + 1
            print(position_courante)

      