#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import  pprint
import  csv
import codecs
import sys 
import  os
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    import  monenv
    import  global_var
else:

    import  V2.monenv
    import  V2.global_var





class RefCSV:
    """Class RefCSV
    """
    # Attributes:
    contenu = None  # (list(tuple)) 
    
    # Operations
    @staticmethod
    def charger(nom_fichier):
        pp             = pprint.PrettyPrinter(indent=4)
        fichier_csv    =None
        RefCSV.contenu = []
        # ws_tmp       = global_var.WORKBOOK_GLOBAL.get_sheet_by_name('resultat_csv')
        row            =None
        
        longueur    = len(monenv.GLOBAL_CHEMIN_CSV)
        longueur_2  = len(monenv.GLOBAL_BASE_SORTIE)
        workbook    = global_var.WORKBOOK_GLOBAL
        #print(monenv.GLOBAL_BASE_SORTIE[0:(longueur_2-1)]+"/"+monenv.GLOBAL_CHEMIN_CSV[1:(longueur-1)])

        nom_fichier_csv=monenv.GLOBAL_CHEMIN_CSV
        try:
            fichier_csv = open(str(nom_fichier_csv), "rw")
       
        #donnees_de_site=fichier_csv.read()
            
            csv_info = csv.reader(fichier_csv,delimiter=';'.encode("latin-1") )
            donnees_de_site = list(csv_info)

            global_var.nombre_ligne_csv = csv_info.line_num



            row=()
            i=0
            inc=0
            #try:
            for i in range(0,global_var.nombre_ligne_csv): 
                inc=1+inc
                RefCSV.contenu.append(donnees_de_site[i])

        #except Exception:
            #    print(donnees_de_site[i])

          

            # ws_tmp['G1'].value="Commentaire"
            # wb.save(monenv.GLOBAL_CHEMIN_XLSX)
            # #wb = load_workbook('resultat.xlsx')
            # ws_tmp = wb.create_sheet("resultat_affichage")
            # wb.active = 1
            # # automatisation creation liste critere 
            # #ws['A1'].fill = redFill
            # ws_tmp['A1'].value = "verification de la SEO"
            # fontObj = Font(size=24, italic=True)
            # ws_tmp['A1'].font = fontObj
            # #ws['A1'].style.font.color = Color(Color.BLUE) 


            # ws_tmp.column_dimensions['A'].width = 45
            # ws_tmp.column_dimensions['C'].width = 45
            
        except:
            print("lancer ./verif_seo.sh avant d'utiliser ce script.")        
        return RefCSV.contenu 
    

