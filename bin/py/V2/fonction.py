#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

def RepresentsInt(s):
    try: 
        if int(s):
            return True
        if s=="N":
            return True
       
    except ValueError:
        return False