#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import sys 

if   sys.version_info.major  == 2:
    from Test import Test
else:
    from V2.Test import Test

global WORKBOOK_GLOBAL
WORKBOOK_GLOBAL=None

global liste_position_erreur
liste_position_erreur=list()

global nombre_total_erreur
nombre_total_erreur=0

global liste_nombre_erreur
liste_nombre_erreur=list()

##
## espacement de tableau et autres chose sur le parametrage
##

global titre_feuille_resultat
titre_feuille_resultat="verification de la SEO"

global espacement_tableau_site
espacement_tableau_site=4

global liste_entree_tableau_site
global liste_entree_tableau_site_correspondance
liste_entree_tableau_site=("erreur 404","sitemap","robot.txt","balise <title> et attribut description","Commentaire","semantique")
liste_entree_tableau_site_correspondance=(Test.rien,Test.erreur_404,Test.sitemap,Test.robot,Test.title,Test.rien,Test.semantique_complexe)

global titre_tableau_stat
titre_tableau_stat="resultat statistique"

global espacement_tableau_stat
espacement_tableau_stat=2

global compteur_erreur_sitemap
compteur_erreur_sitemap=0

global dictionnaire_toute_position
dictionnaire_toute_position={}

global liste_global_site_erreur
liste_global_site_erreur= {'erreur_404':' ','sitemap':' ','robot':' ','title':' ','semantique' : ' ','semantique_complexe' : ' '}

global compteur_erreur_sitemap
compteur_erreur_sitemap=0

global nombre_ligne_csv
nombre_ligne_csv=0