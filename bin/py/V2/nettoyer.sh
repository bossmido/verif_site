#!/usr/bin/env bash

rm ./Cellule.c
rm ./CelluleContenu.c
rm ./CelluleLibelle.c
rm ./Cellule.c
rm ./Couleur.c
rm ./CouleurTerminal.c
rm ./Dessinable.c
rm ./Direction.c
rm ./fonction.c
rm ./global_var.c
rm ./__init__.c
rm ./logger.c
rm ./__main__.c
rm ./monenv.c
rm ./MonStyle.c
rm ./Position.c
rm ./RefCSV.c
rm ./setup.c
rm ./StyleFond.c
rm ./StyleTexte.c
rm ./TableauCSV.c
rm ./TableauGenerique.c
rm ./TableauSiteListe.c
rm ./TableauSite.c
rm ./TableauStat.c
rm ./testPosition.c
rm ./Test.c
rm ./TestStr.c
rm ./XLS.c

#################################

rm ./Cellule.pyc
rm ./CelluleContenu.pyc
rm ./CelluleLibelle.pyc
rm ./Cellule.pyc
rm ./Couleur.pyc
rm ./CouleurTerminal.pyc
rm ./Dessinable.pyc
rm ./Direction.pyc
rm ./fonction.pyc
rm ./global_var.pyc
rm ./__init__.pyc
rm ./logger.pyc
rm ./__main__.pyc
rm ./monenv.pyc
rm ./MonStyle.pyc
rm ./Position.pyc
rm ./RefCSV.pyc
rm ./setup.pyc
rm ./StyleFond.pyc
rm ./StyleTexte.pyc
rm ./TableauCSV.pyc
rm ./TableauGenerique.pyc
rm ./TableauSiteListe.pyc
rm ./TableauSite.pyc
rm ./TableauStat.pyc
rm ./testPosition.pyc
rm ./Test.pyc
rm ./TestStr.pyc
rm ./XLS.pyc

########################################

rm -fr ./site
rm -fr ./..build
rm -fr ./__pycache__
rm -fr ./__main__.build
rm -fr ./__main__.dist
rm ./libbz2.so.1.0
rm ./libcrypto.so.1.0.0
rm ./libssl.so.1.0.0
rm ./libz.so.1
rm ./libncursesw.so.6
rm  ./libreadline.so.7
rm ./libpython2.7.so.1.0
rm ./nohup.out
rm ./test.exe
rm ./..exe
rm ./__main__
rm -fr ./lib
rm -fr ./dist

rm -fr ./.dist
rm -fr ./..dist
rm -fr ./tmp
rm -fr ./build