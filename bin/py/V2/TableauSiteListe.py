#!/usr/bin/env python2.7

# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import  pprint
import sys 
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    import  global_var
    import  monenv

    from    RefCSV             import   RefCSV
    from    TableauGenerique   import   TableauGenerique
    from    TableauSite        import   TableauSite
    from    CelluleLibelle     import   CelluleLibelle
    from    Direction          import   Direction
    from    Position           import   Position
    from    Dessinable import Dessinable

else:

    import  V2.global_var
    import  V2.monenv
    from    V2.Dessinable import Dessinable
    from    V2.RefCSV             import   RefCSV
    from    V2.TableauGenerique   import   TableauGenerique
    from    V2.TableauSite        import   TableauSite
    from    V2.CelluleLibelle     import   CelluleLibelle
    from    V2.Direction          import   Direction
    from    V2.Position           import   Position

class TableauSiteListe(Dessinable):
    """Class TableauCSV
    """
    # Attributes:
    liste_site = []
    feuille_nom = ""
    # Operations

    def __init__(self,feuille_nom):

        self.feuille_nom = feuille_nom

        print("affichage indice")

        global_var.WORKBOOK_GLOBAL.create_sheet(self.feuille_nom)
        inc=-3
        indice_courant=0
        global espacement_tableau_site
        
        for rangee in RefCSV.charger(monenv.GLOBAL_CHEMIN_CSV):
            if indice_courant == 0 or indice_courant == 1 :
                indice_courant = 1 + indice_courant
                continue
            liste_contenu = rangee
            liste_taille_ligne=[]
            for val in liste_contenu:
                liste_taille_ligne.append(150)
                pass
            taille       = 8 # nombre d entre fixe
            nombre_ligne = taille

            self.liste_site.append(TableauSite(feuille_nom,x="A",y=inc,nombre_ligne=nombre_ligne,liste_taille=tuple(liste_taille_ligne),type_entree=Direction.haut_bas,indice=indice_courant))
         
            # if indice_courant==1:
            #     inc = inc +  global_var.espacement_tableau_site
            # else:
            inc = inc + taille + global_var.espacement_tableau_site
            indice_courant = indice_courant + 1
            print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")

            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)
            print(indice_courant)

            print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                
    def dessiner(self,feuille_ou_ecrire="",position=None,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,valeur_a_ajouter=""):
        global espacement_tableau_site
        global titre_feuille_resultat

        inc                 = 0
        inc_indice          = 2

        position_courante   = Position("A",3)
        cellule_titre       = CelluleLibelle(self.feuille_nom,Position("A",1))
        cellule_titre.value = global_var.titre_feuille_resultat
        
#        for test in self.liste_site:
#            print(test.__str__())

        for tableau_site in self.liste_site:
 
            taille = tableau_site.len()

            if tableau_site != None:
                print("affichage du tableau de site")
                print(tableau_site)
                print(taille)

                print("fin affichage du tableau de site")


            tableau_site.dessiner(feuille_ou_ecrire=self.feuille_nom,position=(position_courante+inc),taille=taille,transformation=transformation,couleur=couleur,fond=fond,border=border,indice=inc_indice)
            inc         = inc + taille + global_var.espacement_tableau_site
            inc_indice  = inc_indice + 1