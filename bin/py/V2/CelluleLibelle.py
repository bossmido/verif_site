from __future__ import unicode_literals

import sys 
from    os.path     import dirname, basename, isfile

if   sys.version_info.major  == 2:
    from Cellule    import Cellule
    from Position   import Position
    from Couleur    import Couleur
    from Test       import Test
else:
    from V2.Cellule    import Cellule
    from V2.Position   import Position
    from V2.Couleur    import Couleur
    from V2.Test       import Test




class CelluleLibelle(Cellule):
    """Class CelluleLibelle
    """
    # Attributes:
    libelle = None  # (String) 

    # Operations

    def __init__(self,position_base=Position(),position_relative=Position(),transformation=None,couleur=None,fond=None,border=None):
         
        super(CelluleLibelle,self).__init__(position_base,position_relative,transformation=transformation,couleur=couleur,fond=fond,border=border)
        self.couleur    = None
        self.fond       = "c0c0c0"   
        self.border     = True


    def dessiner_cellule(self,taille=None,transformation=None,couleur=None,fond=None,border=False,indice=0,type_cellule=Test.rien):
        if couleur == None:
            couleur =  self.couleur
        if fond == None:
            fond =  self.fond        
        if border == None:
            borderborder =  self.border

        super(CelluleLibelle, self).dessiner_cellule(transformation=transformation,couleur=couleur,fond=fond,border=border,indice=indice,type_cellule=type_cellule)
        self.dessiner(feuille_ou_ecrire=self.feuille_ou_ecrire,position=self.position_base,taille=taille,transformation=transformation,couleur=couleur,fond=fond,border=border,indice=indice)