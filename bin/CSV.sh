#!/bin/bash 

creer_CSV(){
    if [  -f  $CHEMIN_CSV ]; then
        echo "";
    else 
        touch $CHEMIN_CSV;
        echo -e "url;404notfound;robot.txt;sitemap;description#title;semantique;commentaire;site_libelle\n" > $CHEMIN_CSV;
    fi
}

effacer_CSV(){
    if [  -f  $CHEMIN_CSV ]; then
        rm $CHEMIN_CSV;
    else
        echo "";
    fi
}

ajouter_CSV(){
#     local_array=("$@")
#echo -e $local_array[@];

    # for ((i=1; i <= 8; ++i))
    #     verifier_val "\$$i";
    # done
    local my_1;
    local my_2;
    local my_3;
    local my_4;
    local my_5;
    local my_6;
    local my_7;
    local my_8;
    local my_9;

    if [ -z ${1+x}  ]; then
       my_1="";
    else
        my_1=$1;
    fi   
    if [ -z ${2+x}  ]; then
       my_2="";
    else
       my_2=$2;
    fi   
    if [ -z ${2+x}  ]; then
       my_3="";
    else
        my_3=$3;
    fi   
    if [ -z ${3+x} ]; then
        my_4="";
    else
        my_4=$4;
    fi   
    if [ -z ${4+x}   ]; then
       my_5="";
    else
       my_5=$5;

    fi   
    if [ -z ${5+x} ]; then
       my_6="";
    else
       my_6=$6;
    fi   
    if [ -z ${6+x}  ]; then
       my_7="";
    else
       my_7=$7;
    fi   
    if [ -z ${8+x}  ]; then
       my_8="";
    else
       my_8=$8;
    fi   
    if [ -z ${9+x}  ]; then
       my_9="";
    else
       my_9=$9;
    fi   

    # error=$my_2
    # if [ $2 -gt 0 ]; then
    #     echo -e "$TMP/wget_$my_1";
    #     error="$my_2 $(cat $TMP/wget_$1.log | xargs printf "%q" )";
    # fi 

    sortie=$my_1;
    # if [[ $1 =~ ^www\.(.*) ]]; then 
    #    sortie=${liste_lien[$i]:4:${#liste_lien[$i]}}
    # fi 

    # if [[ $1 =~ ^www1(.*) ]]; then 
    #     sortie=${liste_lien[$i]:5:${#liste_lien[$i]}}
    # fi 
    
    #           1             2     3     4         5                 6             7          8
    #        'site_simple' ; '404';'robot';'sitemap';'description';'semantique';'commentaire';'site'       
    #echo -e  "$sortie;$error;$my_3;$my_4;$my_5;$my_6;;$my_8" >> $CHEMIN_CSV;


    if [ "$#" -gt 0 ]; then
        echo -e ""; 
        #echo -e "$1" >> $CHEMIN_CSV;
    else
        error=$( echo "$error" |  sed -e '{:q;N;s/\n/ /g;t q}' );

       # echo  "$site_libelle;";
        echo  "$site_libelle;$error;$test_robot;$test_sitemap;$test_description;$test_semantique;$commentaire;$lien" >> $CHEMIN_CSV;
    fi 

}



ajouter_CSV_brut(){
    echo -e "************************************\n";
    echo -e "    recherche dans la BDD \n\n";
    echo -e "************************************\n";

    echo -e "$*";

    echo -e "************************************\n";
    echo -e "   fin de recherche dans la BDD \n\n";
    echo -e "************************************\n";
    param_traite=$( echo "$*" | perl -pe 's/\\/\\\\/g'  );
    
    echo -e "$param_traite" >> $CHEMIN_CSV;
}