function stringToUint(string) {
    var string = btoa(unescape(encodeURIComponent(string))),
        charList = string.split(''),
        uintArray = [];
    for (var i = 0; i < charList.length; i++) {
        uintArray.push(charList[i].charCodeAt(0));
    }
    return new Uint8Array(uintArray);
}

function uintToString(uintArray) {
    var encodedString = String.fromCharCode.apply(null, uintArray),
        decodedString = decodeURIComponent(escape(atob(encodedString)));
    return decodedString;
}


function base64ToArrayBuffer (base64) {
  
    var len = base64.length;
    var bytes = new Uint8Array(len);
       var buf = new ArrayBuffer(base64.length); 

    for (var i = 0; i < len; i++) {
        buf[i] =decodeURIComponent( base64).charCodeAt(i);
    }
    return buf;
}

function encode_utf8(s) {
   console.log("%O",s)

   var buf = new ArrayBuffer(s.length); 
   var bufView = new Uint8Array(buf);
   for (var i=0, strLen=s.length; i<strLen; i++) {
     buf[i] = s.charCodeAt(i);
   }
   var tmp_fin =  (new TextDecoder("iso-8859-16")).decode(buf);

   for (var i=0, strLen=tmp_fin.length; i<strLen; i++) {
     buf[i] = tmp_fin.charCodeAt(i);
   }
   console.log("%O",tmp_fin);
   return buf;
}

function str2ab(str) {
   var s = encode_utf8(str)
   var buf = new ArrayBuffer(s.length); 
   var bufView = new Uint8Array(buf);
   for (var i=0, strLen=s.length; i<strLen; i++) {
     buf[i] = s.charCodeAt(i);
   }
   return buf;
 }

var jsonToHtmlTable = jsonToHtmlTable  || {};
function appel(){        
  $('body').html("<h1>OK</h1>"); 
  console.log("test");
}
window.test = jsonToHtmlTable = {
  maj:function(){
    this.afficher_data(null);
  },
  avance : function(){
    this.position++;
    this.maj();
  },
  afficher_par_colonne  : function(data2){
    console.log(data2)
    var keys = Object.keys(data2[0]);
    var longueur_element =  data2.length;
    var nombre_colonnes = keys.length;
    var contenu_table = new Array(nombre_colonnes);
    var num_colonne_curr=0;

    console.log("%O",data2);
    for(nom_th in keys){
      for (var num_colonne_curr = 0; num_colonne_curr<(longueur_element); num_colonne_curr++) {

        //console.log(keys[nom_th])
        //console.log(data2[num_colonne_curr][keys[nom_th]])
       // for( val in data2[num_colonne_curr][keys[nom_th]]){
          if(!(keys[nom_th] in contenu_table )){
            contenu_table[keys[nom_th]] = [];
          }

          if((keys[nom_th] in data2[num_colonne_curr]) && (data2[  num_colonne_curr][keys[nom_th]] !== undefined)){
            contenu_table[keys[nom_th]].push(""+data2[  num_colonne_curr][keys[nom_th]]+""); 
          }
        //}
      }
    }
    console.log(contenu_table);
    console.log(keys);
    /////
    //console.log("%O %O %O %O", keys[0].length,data2,keys,nombre_colonnes,nombre_colonnes,contenu_table)
    var nombre_entree = contenu_table[keys[1]].length ;
    var sortie_ligne=""; 
    for (var j = 0; j < nombre_entree; j++) {
     sortie_ligne+="<tr>";
     for (var i = 0; i < nombre_colonnes; i++) {
      sortie_ligne+="<td>";
      sortie_ligne+=contenu_table[keys[i]][j];
      if(nombre_colonnes-1  < i){
        sortie_ligne+="</td>";
      }
    }
    sortie_ligne+="</tr>";
  }
  sortie_ligne+="";
  return sortie_ligne;
},
afficher_tout: function(){
  this.position=0;
  this.option_json['limite_parsage']=999999;
  this.maj();
},
recule : function(){
  this.position--;
    //alert(this.position)
    this.maj();
  },
  afficher_data : function(param){
    var table = null;
    if( this.plugin){
     // table = this.plugin.destroy()
      //table.fnClearTable(this.plugin);
    }

    if(this.data == null && param != null){
      this.data = param;
    }else{
      $('#' + this.el ).html("");
    }
    if(this.datatables_options["msgpack"]){
      obj_arr = new Uint16Array();
      this.data = windows1252.encode((this.data));
      var t  = new msgpack5();
      var buf = new ArrayBuffer(this.data.length); // 2 bytes for each char
      var bufView = new Uint8Array(buf);

      for (var i=0, strLen=this.data.length; i<strLen; i++) {
            bufView[i] =   String.fromCharCode(this.data.charCodeAt(i)).charCodeAt(0);
      }
      data =  t.decode(bufView);

    }else if(this.datatables_options["yaml"]){
      data = jsyaml.load(this.data);      
    }else{
      console.log(this.data);
      data =  JSON.parse(this.data);
    }
    
    /// data2  = this.traitement_data(data);
    data2  = this.traitement_data(data);

    //console.log(data2);
    var table_head = "<thead><tr>";
    inc_entete=0;
    for( entete in data2[0]){
      ///if(entete  == "site_libelle"){break;}
      table_head += "<th>" + entete + "</th>";
      inc_entete++;

    }
    table_head += "</tr></thead>";
    $('#' + this.el + '').append(table_head);
    $('#' + this.el + '').append("<tbody></tbody>");
    var inc = 0;
    if(this.position == 0)
      data2.shift();
    var inc_retrait=0;
    //console.log(this.option_json['limite_parsage']);
    //console.log((this.position));
    //console.log(data2);
    var tbody_html="";
    if(!!("ordre" in this.datatables_options) || this.datatables_options["ordre"]!="normal" ){
      tbody_html  = this.afficher_par_colonne(data2);
//      console.log(tbody_html)
      //alert(tbody_html);
    }else{
      for(liste_donnee in data2){
        //console.log(this.option_json['limite_parsage']*(this.position),(this.position));
        if((this.option_json['limite_parsage']*(this.position)+1)==inc_retrait){break;}
        data2.shift();
        inc_retrait++;
      }

      for( liste_donnee in data2){
        if((this.option_json['limite_parsage']) == inc){
          break;
        }
        var row_html = "<tr>";
        //takes in an array of column index and function pairs
        if (this.format_special != []) {
          $.each(this.format_special, function(i, v){
            var col_idx = v[0]
            var func = v[1];
            data2[row_id][col_idx]= func(data2[row_id][col_idx]);
          })
        }
          // console.log($("#" + this.el).outerWidth());
          // console.log($(document).outerWidth() );

          //for (col_id = 0; col_id < data[row_id].length; col_id++) { 
            for( liste_attribut in data2[liste_donnee]){
              if(typeof(data2[liste_donnee][liste_attribut]) === "string" || typeof(data2[liste_donnee][liste_attribut]) === "int"  ){
                row_html += "<td>" + data2[liste_donnee][liste_attribut] + "</td>";
              }
            }
            row_html += "</tr>";
         //console.log(row_html );
         $('#' + this.el + ' tbody').append(row_html);
         inc++;
       }
     }
     if(!!("ordre" in this.datatables_options) ||  this.datatables_options["ordre"]!="normal"){
       $('#' + this.el + ' tbody').html(tbody_html);
     }

    //  var table_footer = "<tfoot><tr>";
    //  for( footer_pos in data2[0]){
    //   table_footer += "<td>" + footer_pos + "</td>";
    // }
    // table_footer += "</tr></tfoot>";
    // $('#' + this.el + '').append(table_footer);


    // $('#'+this.el+' tfoot td').each( function () {
    //   var title = $(this).text();
    //   $(this).html( '<input type="text" placeholder="rechercher '+title+'" />' );

    // } );

    if( $("#" + this.el).outerWidth() > ($(document).outerWidth()-50) ){
      $param={"font-size":"12px","max-width":"12px"};
      $("#" + this.el+"  td ul").css({"width":"100%"});
      $("#" + this.el+"  td ul li").css({"border":"0.2px rgba(255,255,255,0.5) solid ","display": "block","width":"100%","padding-bottom":"0","margin-bottom":"0","background":"linear-gradient(#f9f9f9, #e5e5e5)"});
      $("#" + this.el+"  td ul li").mouseover(function(){
        $("#" + this.el+"  td ul li").css({
         "background-color": "none",
         "text-decoration": "none",
         "background":"linear-gradient(#fea, #aef)"
       });
      });
      $("#" + this.el+"  td ul li:odd").css({"background":"linear-gradient(#eee, #fff)"});

      $("#" + this.el+"  td ul li a").css({"word-wrap": "break-word","max-width":"100%","width":"100%"});

      //$("#" + this.el+"  td ul li ").css({"max-width":"12px"});
      //$("#" + this.el+"  td ul").css({"padding-bottom":"0","margin-bottom":"0","background":"linear-gradient(#f9f9f9, #e5e5e5)"});

      $("#" + this.el+"  td ul li").css({"padding-bottom":"0","margin-bottom":"0"});
      $("#" + this.el+"  td ul li a").css({"padding-bottom":"0","-bottom":"0"});

      $("#" + this.el+" td,th").css($param);
      $("#" + this.el+" .conteneur_ul").css({"width":"100%","height":"100%","word-wrap": "break-all","margin":"0","padding":"0"});
      $("#" + this.el+" :has(.conteneur_ul)").css({"margin":"0","padding":"0"});
      $("#" + this.el+" td,th").css($param);
      $("#" + this.el+" ul li,ul").css({"font-size":"9px","display" : "inline-block","padding":"0","margin":"0","text-align" : "left"});
      $("#" + this.el+" :has(td) ul li,:has(td) ul").css({"word-wrap": "break-all"});
      
      $("#" + this.el+" td  ul").css({"word-wrap": "break-all"});

      $("#" + this.el+" td:not(:nth-child(6)):not(:nth-child(7))").css({"text-align":"center","vertical-align":"middle"});
    }
    //console.log("test" );

    //console.log($('#' + this.el + ''));
    // this.datatables_options['aoColumns'] = [
    //   { "sWidth": "45%", "bSearchable" : false, "bSortable" : false },
    //   { "sWidth": "45%", "bSearchable": false, "bSortable": false },
    //   { "sWidth": "10%", "bSearchable": false, "bSortable": false },
    //   { "sWidth": "10%", "bSearchable": false, "bSortable": false },
    //   { "sWidth": "10%", "bSearchable": false, "bSortable": false },
    //   { "sWidth": "10%", "bSearchable": false, "bSortable": false },
    //   { "sWidth": "10%", "bSearchable": false, "bSortable": false }

    // ];
 //   var plugin  = window.plugin  = this.plugin = $('#' + this.el + '').DataTable(this.datatables_options);
    // plugin.columns().every( function () {
    //   var that = this;

    //   $( 'input', this.footer() ).on( 'keyup change', function () {
    //     if ( that.search() !== this.value ) {
    //       that
    //       .search( this.value )
    //       .draw();


    //     }

    //   });
    // });

    if((table) &&  (table.plugin != undefined ) ) {
      table.plugin.fnDraw();
    }

    if (this.datatables_options)
      $("#" + this.el).append("<p><a class='btn btn-info' href='" + this.chemin_fichier + "'><i class='glyphicon glyphicon-download'></i> Télécharger le JSON</a></p>");
   // console.log("#" + this.el);
      // }
//        $("#" + el).html(data["html"]);

},
traitement_data : function(data){
  for( liste_donnee in data){
    for( liste_attribut in data[liste_donnee]){
              ///debugger;
              //console.log( liste_attribut );
              if(
                liste_attribut == "robotnotfound" ||
                liste_attribut == "sitemap"   ||
                liste_attribut == "description#title" 
                ){
                if(data[liste_donnee][liste_attribut] == "OK"){
                  data[liste_donnee][liste_attribut] ="<span style='color : green;'>"+data[liste_donnee][liste_attribut]+"<span>";
                }else{
                  data[liste_donnee][liste_attribut] ="<span style='color : red;'>"+data[liste_donnee][liste_attribut]+"<span>";   
                }
              }
              if(liste_attribut.match(/(.*)url(.*)/)){
                //data[liste_donnee][liste_attribut] =data[liste_donnee][liste_attribut].replace(/_/g," ");

                if(data[liste_donnee][liste_attribut].match(/(.*)V6(.*)/)){
                  data[liste_donnee][liste_attribut] =data[liste_donnee][liste_attribut].replace(/V6/g,"<span style='color : green;'>V6</span>");
                }

                if(data[liste_donnee][liste_attribut].match(/(.*)V5(.*)/)){
                 data[liste_donnee][liste_attribut] =data[liste_donnee][liste_attribut].replace(/V5/g,"<span style='color : cyan;'>V5</span>")
               }
             }

             if(liste_attribut.match(/(.*)semantique(.*)/)){
              data[liste_donnee][liste_attribut] =data[liste_donnee][liste_attribut].replace(/_/g," ");
            }


            if(liste_attribut.match(/(.*)commentaire(.*)/)){
              if(data[liste_donnee][liste_attribut] =="commentaire" ){
                data[liste_donnee][liste_attribut] = "<span style='color : #aaa;'>Commentaire</span>"
              }              
            }


            if(liste_attribut.match(/(.*)site_libelle(.*)/)){
              data[liste_donnee][liste_attribut] = "";
            }

            if(liste_attribut.match(/(.*)url(.*)/)){
              //if(data[liste_donnee][liste_attribut] =="commentaire" ){
                data[liste_donnee][liste_attribut] = "<a  target='_blank' href='http://"+data[liste_donnee]["site_libelle"]+"'>"+data[liste_donnee][liste_attribut]+"</a>"
              //}              
            }
            if(liste_attribut.match(/(.*)404notfound(.*)/)){
             var sortie_404 =0;


            ///var sortie_404 = "<div> nombre de fichiers manquants : "+data[liste_donnee][liste_attribut]['nombre_erreur']+"</div>";
            if(data[liste_donnee][liste_attribut]['nombre_erreur']>1){
              sortie_404="<div class='conteneur_ul'> nombre de fichiers manquants : "+data[liste_donnee][liste_attribut]['nombre_erreur']+"</div>";
              sortie_404 += "<ul>";
              var inc_sous_tab=0; 
              for (var i = 0; i < (data[liste_donnee][liste_attribut]['fichier'].length); i++) {
                sortie_404 += "<li><a href='"+data[liste_donnee][liste_attribut]['fichier'][i]+"'>"+data[liste_donnee][liste_attribut]['fichier'][i]+"</a></li>";    
              }

              sortie_404 += "</ul>";
            }
            if(data[liste_donnee][liste_attribut]['moins_un'] == -2){
              sortie_404="<div>  le site est peut-etre injoignable</div>";
            }else{
                //  
              }
              if(sortie_404 ==0){
                sortie_404="aucun probleme de lien";
              }
              data[liste_donnee][liste_attribut] = sortie_404;

              //console.log(              data[liste_donnee][liste_attribut]);


            }else{
   //                       data[liste_donnee][liste_attribut] = sortie_404;

 }

              //data[liste_donnee][liste_attribut];
            }
          }


         // var obj_retour =  $.extend(null, {},data);
        //  var retour_list = $.map(obj_retour, function(value, index) {return [value];    });
        return data.clone();
      },

      init: function (options) {
        Array.prototype.clone = function() {
          return this.slice(0);
        };
        this.plugin=null;
        this.data=null; 
        options = options || {};
        this.position=0;
        this.data=null;
        this.chemin_fichier = options.chemin_fichier || "test.json";
        this.el = options.element || "table-container";
        this.option_json = options.option_json || {};
        this.datatables_options = options.datatables_options || {};
        this.format_special = options.format_secial || [];
         // alert(this.option_json['limite_parsage']);

         if(this.datatables_options["msgpack"] == true){
          this.chemin_fichier+=".msg";
        }else if(this.datatables_options["yaml"] == true){  
          this.chemin_fichier+=".yaml";
        }else{
          this.chemin_fichier+=".json";
        }
        var type_requete="text";
        if(this.datatables_options["msgpack"]){
          type_requete="text";
        }

        $.ajax({
          type : "GET",
          cache: false,
          contentType: "application/x-www-form-urlencoded;charset=utf-8",
          dataType: type_requete,
      contentType: 'application/json; charset=utf-8',
     cache: false,
     timeout: 30000,
     url: this.chemin_fichier,
     data: {},
     success: function(data){
        this.afficher_data(data);
        //console.log(data);
        // handleError(xhr, status, ''); // manually trigger callback
        // appel();

      }.bind(this),
      error:function(){alert("probleme");},
      done : function(data){console.log(textStatus);}
    });

        var oLanguage=     {
          sSortAscending: ": activer pour trier dans l'ordre ascendant",
          sSortDescending: ": activer pour trier dans l'ordre descendant",

          oPaginate: {
            sFirst: "premier",
            sLast: "dernier",
            sNext: "suivant",
            sPrevious: "précédant"
          },
          sEmptyTable: "Pas de donnée dans la table",
          sInfo: " _START_ à  _END_ de _TOTAL_ entrées",
          sInfoEmpty: " 0 à  0 de 0 entrées",
          sInfoFiltered: "(filtré _MAX_ entrées au total)",
          sInfoPostFix: "",
          sDecimal: "",
          sThousands: ",",
          sLengthMenu: "Montrer _MENU_ entrées",
          sLoadingRecords: "Chargement...",
          sProcessing: "Execution de tâche...",
          sSearch: "Recherche:",
          sSearchPlaceholder: "",
          sUrl: "",
          sZeroRecords: "pas d'élément correspondant"
        };
        $.fn.dataTable.ext.errMode = 'none';
        this.datatables_options['oLanguage']=oLanguage;
        this.datatables_options['errMode']="non";
        this.datatables_options['sErrMode']="non";
      }
    }