# Python http.server that sets Access-Control-Allow-Origin header.
# https://gist.github.com/razor-x/9542707

import os
import re

import sys
import http.server
import socketserver
import pprint
PORT = 8000

class HTTPRequestHandler(http.server.SimpleHTTPRequestHandler):

    def end_headers(self):


        if  re.match("(.*)\.msg(.*)",self.path)>0:
            pprint.pprint(self.path)
            pprint.pprint(self.path)
            pprint.pprint(self.path)
            pprint.pprint(self.path)
            pprint.pprint(self.path)
            #self.send_response(200)
            self.send_header('Content-Type','application/msgpack; charset=binary')
            http.server.SimpleHTTPRequestHandler.end_headers(self)
            return
        self.send_header("Cache-Control", "no-cache, no-store, must-revalidate")
        self.send_header("Pragma", "no-cache")
        self.send_header("Expires", "0")

        self.send_header('Access-Control-Allow-Origin', '*')
        http.server.SimpleHTTPRequestHandler.end_headers(self)

def server(port):
    httpd = socketserver.TCPServer(('', port), HTTPRequestHandler)
    return httpd

if __name__ == "__main__":
    port = PORT
    httpd = server(port)
    try:
        print("\nserving from build/ at localhost:" + str(port))
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("\n...shutting down http server")
        httpd.shutdown()
        sys.exit()