#!/bin/bash 
set -a


# export chemin_csv="${BASE_SORTIE}/resultat.csv";
# export chemin_xlsx="${BASE_SORTIE}/resultat.xlsx";
export CHEMIN_CSV="${BASE_SORTIE}/resultat.csv";
export CHEMIN_XLSX="${BASE_SORTIE}/resultat.xlsx";
export CHEMIN_XLS="${BASE_SORTIE}/resultat.xls";

export SOURCE="${BASE_SOURCE}/sans_wysiup.txt ${BASE_SOURCE}/site_wysiup.txt";
