#!/bin/bash 
set -a


############################################################################################
export BASE_DIR=$(pwd);
############################################################################################

export TMP="tmp";
export BASE_SOURCE="sites";
export BASE_BIN="bin";
export BASE_BIN_PY="${BASE_BIN}/py";
export BASE_SORTIE="sortie";
export BASE_VENDOR="${BASE_BIN}/vendor";
export BASE_MES_LOG="$TMP/log";
export BASE_SORTIE_HTML="${BASE_BIN}/json_to_table";
