REM  *****  BASIC  *****

Sub Main

End Sub

Private Sub Worksheet_Change(ByVal Target As Range)

    If Target.Address = Range("A1").Address Then

        ' Get the last row on our destination sheet (using Sheet2, col A here)...
        Dim intLastRow As Long
        intLastRow = Sheet2.Cells(Sheet2.Rows.Count, "A").End(xlUp).Row

        ' Add our value to the next row...
        Sheet2.Cells(intLastRow + 1, "A") = Target.Value

    End If

End Sub
