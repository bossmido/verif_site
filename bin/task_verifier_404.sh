#!/bin/bash

task_verifier_404(){
    retour_bool=$($WGET $WGET_OPTION   -A html,css,js,htm  -O -    $1 --timeout 50 |  cat );
    retour_taille=$($WGET $WGET_OPTION -O - $1 | $PUP  'body' |  cat | wc -c); 
    # test_test=$( [ $retour_taille -gt 405 ] );
    # test_test2=$( [ -n $retour_bool ]);
   
    if  [[ -n $retour_bool ]] ; then
        if [[ $retour_taille -gt 555 ]] ; then
            if [[ ! -f $TMP/$1_wget.log ]]; then
                string_test_404=$( $WGET $WGET_OPTION -o $TMP/$1_wget.log -w 1  -r $level_recursif -p  $1; cat $TMP/wget_$1.log | grep -o "échec"| wc -cl );
                nombre_404=$(( $( echo -e $affiche_log | grep -o "\[" | wc -l ) -1 ));    
                nombre_404=""
                echo -e "${nombre_404} ${string_test_404}";
            else
                string_test_404=$(cat $TMP/$1_wget.log | grep "échec"| wc -c );
                log=$(cat $TMP/$1_wget.log | grep "échec" );
                string_test_404=$(echo "$string_test_404" | tr -d '\n'   );
                nombre_404=$(( $( echo -e $affiche_log | grep -o "\[" | wc -l ) -1 ));    
                echo -e "${nombre_404} ${string_test_404}";
            fi
    
            # echo -e "mtest du site :";
            # echo -e "test de parsage de wget.log";
            # echo -e "\n\e[0m";

            affiche_log=`./bin/py/parse_wget.py  tmp/$1_wget.log tmp/log/$1_wget.log`;
            affiche_log_2=$( echo "$affiche_log" | tr -d '\n'   );
            nombre_404=$(( $( echo -e $affiche_log | grep -o "u'" | wc -l ) ));    


            # if  [[  $nombre_404 -ne 0 ]] ; then
            #     nombre_404=$(( $nombre_404  - 1 ));
            # fi
            echo -e "${nombre_404} ${affiche_log}"; 

        else
            echo -e "-2";
        fi
    else
        echo -e "-1";  
    fi
}
