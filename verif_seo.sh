#!/bin/bash 

#set -x -e

    #$title_vide=$(wget  -q -O- $1 | eval $PUP "title[description]:empty");

############################################################################################
# configuration non stable
############################################################################################

export CACHE=0;
export TRI=0;
export level_recursif=5;
export DEBUG=0;



############################################################################################
# chargement de la configuration
############################################################################################
export CONFIG="bin/config";
source "./${CONFIG}/chemin.sh";
source "./${CONFIG}/chemin_fichier.sh";
source "./${CONFIG}/programme.sh";

source  "${BASE_BIN}/fonction.sh";


# if [ -v "$1" ]
#     then
#     echo "changement de niveau récursivité";
#     level_recursif=$1
# fi

############################################################################################
# chargement script d'entree/sortie
############################################################################################
charger_script "CSV.sh";
charger_script "sqlite.sh";

declare -a liste_task=();

############################################################################################
charger_script "task_fonction.sh";

charger_script "task_verifier_404.sh";
charger_script "task_verifier_version.sh";
charger_script "task_verifier_sitemap.sh";
charger_script "task_verifier_robot.sh";
charger_script "task_verifier_semantique.sh";
charger_script "task_verifier_title.sh";


############################################################################################

echo -e "************************************\n";
echo -e "traitement de la SEO des sites      \n";
echo -e "************************************\n";

export liste_site_complet=$(cat $SOURCE);

touch $TMP/tmp.html;

if [ ! -d $TMP/log ]; then
   mkdir $TMP/log;
fi

log "chemin tmp";
log $TMP;
log "fin chemin tmp";

export duree_complete=0;
export interval=0;
if [[ $1 != "test" ]] ; then

    effacer_CSV;
    creer_CSV;
############################################################################################
#partie tri du CSV
############################################################################################
charger_script tri_liste_csv.sh;
############################################################################################

#verif lien mort
num_site=1;

for lien in $liste_site_complet; do
    sync;
    test_BDD=$(./${BASE_BIN_PY}/BDD/test_BDD.py $lien);
    #if ( test $([ "0" ==  "${test_BDD}" ] ) -a $( test $CACHE -eq 1)) ; then

    #test_existe=$(( echo -e "$ttest_BDD"  |  grep -o "no such table"   | wc -c ));
    test_existe=$(( $( echo -e $test_BDD | grep -o "\[" | wc -l )  ));    

    if    [[ !  "$test_BDD" == "0" ]]  ; then
        insertion_BDD=$(./${BASE_BIN_PY}/BDD/BDD.py $lien);
    fi
    #if  [ $test_BDD  -a  ! $CACHE ] ; then
        echo -e "************************************\n";
        echo -e "    debut debug                     \n";
        echo -e "************************************\n";

        #echo -e "$test_existe"
        echo -e " test_BDD           : $test_BDD"
        echo -e " CACHE              : $CACHE"
        $( [[ $test_BDD == "0" ]] );
        res_ok=$?
        echo -e " test sur test_BDD  : $res_ok"
        
        $(  [[   "$test_BDD" != "0" ]]   );
        res_pas_ok=$?
        echo -e " test complet       : $res_pas_ok"

        echo -e "************************************\n";
        echo -e "    fin debug                       \n";
        echo -e "************************************\n";
    #fi
    if [[ $res_ok -ne 1 ]]; then
        #if [[ "$test_existe" != "0" ]]; then
        #
        echo -e "********************************************************************\n";
        echo -e "   traitement du site $lien  ( $num_site / $(echo -e $liste_site_complet | tr " " "\n"  | uniq -c | sort -bgr  | wc -l)  ) \n";
        echo -e "********************************************************************\n";

        declare -a l=();
        export test_404=""
        export test_sitemap=""
        export afficher_label="" 
        export test_sitemap=""
        export test_robot=""
        export test_description=""
        export test_semantique=""
        export site_libelle=$lien;

        afficher_label $lien "test d'erreur 404";
        export test_404=$(task_verifier_404 $lien);

        #test_404=escape_sed test_404;
        afficher_label $lien "test de balise description dans title manquante";
        export test_description=$(task_verifier_title $lien);
        #log2 " test de description $test_description";
        export test_sitemap=$(task_verifier_sitemap $lien);
        afficher_label  $lien "test de fichier sitemap manquant";
        version=$(task_verifier_version $lien);
        export test_robot=$(task_verifier_robot $lien);
        afficher_label $lien "test de problème de contenu sémantique de la page";
        export test_semantique=$(task_verifier_semantique $lien);
        ######
        if [[ ${lien} =~ ^www\.(.*) ]]; then 
           site_libelle="${lien:4:${#lien}}";
        fi 

        if [[ ${lien} =~ ^www1(.*) ]]; then 
            site_libelle="${lien:5:${#lien}}";
        fi 
        ######
        export site_libelle="$site_libelle $version";
        export commentaire="commentaire";

        export error=$test_404
        if [[ $2 -gt 0 ]]; then
            echo -e "$TMP/wget_$lien";
            error="$my_2 test $(cat $TMP/log/$lien_wget.log | xargs printf "%q" )";
            error=$(echo "$error" |  sed -e '{:q;N;s/\n/ /g;t q}'  );

        fi 
        echo -e "retélécharge des fichiers de site non traités";
        echo -e "$lien";
        echo -e "ajout dans le CSV \n";

        #                  1           2          3          4                5               6               7         8
        #ajouter_CSV $site_libelle $test_404 $test_robot $test_sitemap $test_description $test_semantique $commentaire $lien;
        ajouter_CSV;
        ajouter_sqlite;
       
        #maj du xls
    else
        echo -e "************************************\n";
        echo -e "  recherche dans la BDD de  $lien   \n";
        echo -e "************************************\n";

        export resultat=$(python "./${BASE_BIN_PY}/BDD/chercher_BDD.py" $lien );

        if [[ "$resultat" == "-1" ]]; then 
            echo "erreur de recherche d un site qui nexiste meme pas dans la BDD";
            exist 1;

        fi 

        ajouter_CSV_brut $resultat;
    fi

        if [[ "$1" == "test" ]]; then 
            echo -e "./${BASE_BIN_PY}/V2/__main__.py";
            (nohup python "./${BASE_BIN_PY}/V2/__main__.py" > $TMP/script.out 2>&1  &);

        else
            (nohup python "./${BASE_BIN_PY}/creer_xls.py" > $TMP/script.out 2>&1  &);
        fi 

        num_site=$((num_site + 1));
        
done

############################################################################################
#partie tri du CSV
############################################################################################
charger_script tri_csv.sh
############################################################################################

############################################################################################
#script de fin de conversion de CSV et d'affichage
############################################################################################
charger_script script_fin.sh;
fi
rm ./bin/json_to_table/resultat.json ;
rm ./bin/json_to_table/resultat_tab.json; 
(python "./${BASE_BIN_PY}/csvtojson.py" > "${BASE_DIR}/${BASE_SORTIE}/resultat.json" 2>&1  );
(python "./${BASE_BIN_PY}/csvtojson.py" > ./bin/json_to_table/resultat.json 3>&1  );

#(python "./${BASE_BIN_PY}/jsonstream2jsonarray.py" > "${BASE_DIR}/${BASE_SORTIE}/resultat_tab.json" 2>&1  );
(python "./${BASE_BIN_PY}/jsonstream2jsonarray.py"  --tab > ./bin/json_to_table/resultat_tab.json 2>&1  );

pwd_pre_server=$(pwd);
cd "./bin/json_to_table";
rm ./resultat_tab.msg;
rm ./resultat.msg
#echo -e   "${BASE_DIR}/${BASE_VENDOR}/json2msgpack"  -i  "./resultat_tab.json" -o resultat_tab.msg
("${BASE_DIR}/${BASE_VENDOR}/json2msgpack" -b -B  1 -i "./resultat_tab.json" -o resultat_tab.msg  2>&1  );
("${BASE_DIR}/${BASE_VENDOR}/json2msgpack" -b -B 1  -i "./resultat.json" -o resultat.msg  2>&1  );


#("${BASE_DIR}/${BASE_VENDOR}/json2msgpack"  -i "./resultat.json" -o resultat.msg  2>&1  );

(nohup python "./server.py" 2>&1  &);
cd ${pwd_pre_server};


#ssconvert $BASE_DIR/resultat.csv $BASE_DIR/resulat.odf -T Gnumeric_OpenCalc:openoffice
python ./${BASE_BIN_PY}/V2/;    sleep 2 ;
#if [ -f "./${BASE_SORTIE}/resultat.xlsx" ]; then

    chmod 777  "./${BASE_SORTIE}/resultat.xlsx";
if [[ $1 != "test" ]] ; then
    
    if hash ssconvert 2>/dev/null; then
        ssconvert --export-type=Gnumeric_Excel:excel_dsf "./${BASE_SORTIE}/resultat.xlsx" "./${BASE_SORTIE}/resultat.xls";
        soffice ${BASE_SORTIE}/resultat.xls --norestore --nologo 2>&1 & 

    fi
fi
erreur_txt=$(./${BASE_BIN}/vendor/xls2txt -f ./${BASE_SORTIE}/resultat.xls | grep 'KO' );
echo -e "$erreur_txt";


############################################################################################
#message de fin
############################################################################################
echo -e "###############################################################################\n";
echo -e " Serveur pour l'analyse des tableau sur \e[1m http://127.0.0.1:8000 \e[0m \n";
echo -e "###############################################################################\n\n";
############################################################################################

echo -e "$BASE_BIN_PY"
echo -e "`pwd`"

(python "${BASE_DIR}/${BASE_BIN_PY}/json2yaml.py" "./$BASE_SORTIE_HTML/resultat.json" > "./$BASE_SORTIE_HTML/resultat.yaml"  2>&1  );
(python "${BASE_DIR}/${BASE_BIN_PY}/json2yaml.py" "./$BASE_SORTIE_HTML/resultat_tab.json"   > "./$BASE_SORTIE_HTML/resultat_tab.yaml"  2>&1  );