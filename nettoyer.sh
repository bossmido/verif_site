#!/bin/bash 

export CONFIG="bin/config";
source "$CONFIG/chemin.sh";
source "$CONFIG/chemin_fichier.sh";
source "$CONFIG/programme.sh";

source  "${BASE_BIN}/fonction.sh";

rm  ./tmp/log/my.log;
rm  ./resultat.{*};
# rm -fr ./tmp;
# rm -fr ./\'tmp;
rm  ./nohup.out;
rm  ./script.out;
rm  ./verif_site.sublime-workspace;
rm  ./tmp/script.out;
rm  ./sortie/*;
rm  ./tmp/log/*;

rm "./bin/json_to_table/resultat"*;

rm  ./**/*.rej;
rm  ./**/*.orig;

rm  V2.exe;
