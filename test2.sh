#!/bin/bash 

#set -x -e
shopt -s extglob

##################################

affiche_log=`./bin/py/parse_wget.py  tmp/$1_wget.log tmp/log/$1_wget.log`;
affiche_log_2=$( echo "$affiche_log" | tr -d '\n'   );
nombre_404=$(( $( echo -e $affiche_log | grep -o "u'" | wc -l ) ));    
echo -e "${nombre_404} ${affiche_log}"; 